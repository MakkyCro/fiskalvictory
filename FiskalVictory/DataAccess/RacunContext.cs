﻿using FiskalVictory.MVVM.Models;
using Microsoft.EntityFrameworkCore;

namespace FiskalVictory.DataAccess
{
    public class RacunContext : DbContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("Data Source=podaci.db");
            optionsBuilder.UseLazyLoadingProxies();
            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Normativ>().HasKey(o => new { o.sifra_normativa, o.sifra_artikla });
            modelBuilder.Entity<Stavka>().HasKey(o => new { o.rb_stavke, o.sifra_racuna });
        }

        public DbSet<Artikl> Artikli { get; set; }
        public DbSet<JedinicaMjere> Jed_mj { get; set; }
        public DbSet<Grupa> Grupa { get; set; }
        public DbSet<Kategorija> Kategorija { get; set; }
        public DbSet<Normativ> Normativi { get; set; }

        public DbSet<Radnik> Radnici { get; set; }

        public DbSet<Racun> Racuni { get; set; }
        public DbSet<Stavka> Stavke { get; set; }
        public DbSet<NacinPlacanja> NacinPlacanja { get; set; }
    }
}
