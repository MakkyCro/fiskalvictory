﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Controls;
using System.Windows.Threading;

namespace FiskalVictory.Core
{
    public class ClockManager
    {

        public static readonly ClockManager clock = new ClockManager();

        private ClockManager() { }

        public static ClockManager Clock
        {
            get
            {
                return clock;
            }
        }

        TextBlock tb_time;
        TextBlock tb_date;

        // Pokretanje timera za osvježavanje vremena i datuma
        public void startTimer(TextBlock tb_t, TextBlock tb_d)
        {
            //Izrada novog timera
            //Interval osvježavanja je sekunda
            //Dodaj metodu za tick
            //Pokreni timer
            tb_time = tb_t;
            tb_date = tb_d;
            DispatcherTimer timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromSeconds(1);
            timer.Tick += timer_Tick;
            timer.Start();
        }
        void timer_Tick(object sender, EventArgs e)
        {
            // Osvježi vrijeme
            tb_time.Text = DateTime.Now.ToString("HH:mm:ss");
            // Osvježi datum 
            // Uzmi tri slova od dana, prevedi na hrvatski i dodaj ostatku datuma
            string day = DateTime.Now.ToString("ddd", new System.Globalization.CultureInfo("hr-HR")).ToUpper();
            tb_date.Text = day + DateTime.Now.ToString(", dd. MMMM yyyy.", new System.Globalization.CultureInfo("hr-HR"));
        }

    }
}
