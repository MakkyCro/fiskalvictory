﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace FiskalVictory.Core
{
    public sealed class QueryBuilder
    {
        public static readonly QueryBuilder queryBuilder = new QueryBuilder();
        private QueryBuilder() { }

        public static QueryBuilder QueryManagment
        {
            get { return queryBuilder; }
        }
        public string buildInsertQuery(string db, string table, string[] fields, string[] values)
        {
            string sqlCommand = "INSERT INTO `" + db + "`.`" + table + "` (";

            string lastField = fields.Last();
            foreach (string field in fields)
            {
                if (field.Equals(lastField))
                {
                    sqlCommand += "`" + field + "`) VALUES (";
                }
                else
                {
                    sqlCommand += "`" + field + "`, ";
                }
            }

            string lastValue = values.Last();
            foreach (string value in values)
            {
                if (value.Equals(lastValue))
                {
                    sqlCommand += "'" + value + "');";
                }
                else
                {
                    sqlCommand += "'" + value + "', ";
                }
            }

            return sqlCommand;
        }
    }
}
