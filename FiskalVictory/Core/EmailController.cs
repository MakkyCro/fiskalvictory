﻿using FluentEmail.Core;
using FluentEmail.Smtp;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Net.Mail;
using System.Net;
using System.Text;
using System.Windows;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace FiskalVictory.Core
{
    public class EmailController
    {
        public async Task sendEmail(string emailTo, string emailUserName, string emailSubject, string emailBody, string[] emailAttachments = null )
        {
            try
            {
                var emailsender = new SmtpSender(() => new SmtpClient("smtp.gmail.com")
                {
                    UseDefaultCredentials = false,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    EnableSsl = true,
                    Port = 587,
                    Credentials = new NetworkCredential(userdata.Default.gmail, userdata.Default.gmail_pass)
                });
                Email.DefaultSender = emailsender;

                var email = Email
                    .From("fiskalvictory@gmail.com")
                    .To(emailTo, emailUserName)
                    .Subject(emailSubject)
                    .Body(emailBody);

                if(emailAttachments != null)
                    if (emailAttachments.Length > 0)
                        foreach (string attachment in emailAttachments)
                            email.AttachFromFilename(attachment);
                   
                await email.SendAsync();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }
        }
    }
}
