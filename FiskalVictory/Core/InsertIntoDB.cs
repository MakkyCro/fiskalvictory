﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;

namespace FiskalVictory.Core
{
    public class InsertIntoDB
    {
        public static readonly InsertIntoDB insertIntoDB = new InsertIntoDB();
        private InsertIntoDB() { }

        public static InsertIntoDB InsertIntoDBManager
        {
            get { return insertIntoDB; }
        }

        string connectionDB = ConfigurationManager.ConnectionStrings["FV_DB"].ToString();
        public void insert(string query)
        {
            try
            {
                // Izrada konekcije  
                MySqlConnection connection = new MySqlConnection(connectionDB);
                // Izrada nove komande za izvršavanje  
                MySqlCommand comm = new MySqlCommand(query, connection);
                MySqlDataReader reader;
                connection.Open();

                // Izvršavanje querya i zapisivanje u bazu podataka
                reader = comm.ExecuteReader(); 

                connection.Close();
            }
            catch
            {}
        }

    }
}
