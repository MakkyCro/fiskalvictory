﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;

namespace FiskalVictory.Core
{
    public class Location
    {
        public static readonly Location location = new Location();

        private Location() { }

        public static Location LocationManager
        {
            get
            {
                return location;
            }
        }

        SelectFromDB getFromDB = SelectFromDB.GetFromDataBase;

        public List<DataRow> drzave;
        public List<DataRow> zupanije;
        public List<DataRow> opcine;
        public List<DataRow> mjesta;

        public string tmp_drzava_id;
        public string tmp_zupanija_id;
        public string tmp_opcina_id;
        public string tmp_mjesto_id;


        // Čitanje baze podataka - sve države, županije, općine i mjesta
        public void LoadData()
        {
            drzave = getFromDB.selectAll("SELECT * FROM fiskalvictory.drzava;").AsEnumerable().ToList();
            zupanije = getFromDB.selectAll("SELECT * FROM fiskalvictory.zupanija;").AsEnumerable().ToList();
            opcine = getFromDB.selectAll("SELECT * FROM fiskalvictory.opcina ;").AsEnumerable().ToList();
            mjesta = getFromDB.selectAll("SELECT * FROM fiskalvictory.mjesto;").AsEnumerable().ToList();
        }

        // Učitavanje vrijednosti u comboboxeve
        public void LoadIntoCB(ComboBox cb, List<DataRow> data, int field_index)
        {
            foreach (DataRow row in data)
            {
                cb.Items.Add(row[field_index].ToString());
            }
        }

        // Dobivanje indexa drzave, zupanije, opcine i mjesta za kasnije unošenje podataka u bazu
        public string[] getLocationIDs (string drzava, string zupanija, string opcina, string mjesto)
        {
            DataRow selected_drzava = drzave.Find(row => row[1].Equals(drzava));
            DataRow selected_zupanija = zupanije.Find(row => row[2].Equals(zupanija));
            DataRow selected_opcina = opcine.Find(row => row[3].Equals(opcina));
            DataRow selected_mjesto = mjesta.Find(row => row[4].Equals(mjesto));

            string d = selected_drzava[0].ToString();
            string z = selected_zupanija[1].ToString();
            string o = selected_opcina[2].ToString();
            string m = selected_mjesto[3].ToString();

            return new[] { d,z,o,m };
        }

        public string getLocationNameByID(List<DataRow> data,int column, int id)
        {
            return data.Find(row => row[column].Equals(id))[column+1].ToString();
        }

        // Osvježavanje podataka kada odaberemo županiju
        public void ZupanijaChanged(string selected_zupanija, ComboBox cb_opcina, ComboBox cb_mjesto)
        {
            // Brisanje općina i mjesta
            cb_opcina.SelectedItem = null;
            cb_opcina.Items.Clear();
            cb_mjesto.SelectedItem = null;
            cb_opcina.Items.Clear();
            cb_mjesto.IsEnabled = false;

            // Dobivanje retka u kojem se nalazi odabrana županija 
            // Radimo kako bismo došli do ID županije
            var tmp = zupanije.Find(row => row[2].Equals(selected_zupanija));

            // Postavljanje id županije za točnije biranje mjesta
            tmp_zupanija_id = tmp[1].ToString();

            // Filtriranje općina prema ID-u županije
            foreach (DataRow row in opcine)
            {
                if (row[1].ToString() == tmp[1].ToString())
                {
                    cb_opcina.Items.Add(row[3].ToString());
                }
            }

            // Omogućavanje korištenja comboboxa za općine
            cb_opcina.IsEnabled = true;
        }

        public void OpcinaChanged(string selected_opcina, ComboBox cb_mjesto)
        {
            // Brisanje mjesta
            cb_mjesto.SelectedItem = null;
            cb_mjesto.Items.Clear();
            
            // Dobivanje retka u kojem se nalazi odabrana općina
            var tmp = opcine.Find(row => row[3].Equals(selected_opcina));

            // Filtriranje podataka o mjestima
            foreach (DataRow row in mjesta)
            {
                if (row[2].ToString() == tmp[2].ToString() && row[1].ToString() == tmp_zupanija_id)
                {
                    cb_mjesto.Items.Add(row[4].ToString());
                }
            }

            // Omogućavanje korištenja comboboxa za mjesta
            cb_mjesto.IsEnabled = true;
        }
    }
}
