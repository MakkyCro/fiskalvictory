﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows;
using DeviceId;


namespace FiskalVictory.Core
{
    public sealed class Licence
    {
        public static readonly Licence lic = new Licence();

        private Licence() { }

        public static Licence Lic
        {
            get { 
                return lic; 
            }
        }

        SelectFromDB getFromDB = SelectFromDB.GetFromDataBase;
        InsertIntoDB insertDB = InsertIntoDB.InsertIntoDBManager;
        QueryBuilder queryController = QueryBuilder.QueryManagment;

        // Generiranje licence
        public void generateDeviceID()
        { 
            string deviceId = new DeviceIdBuilder()
            .AddMachineName()
            .AddMacAddress()
            .AddProcessorId()
            .AddMotherboardSerialNumber()
            .ToString();

            userdata.Default.id_uredaja = deviceId;
            userdata.Default.Save();
            userdata.Default.Reload();
        }

        // Provjera postoji li već generirana licenca u bazi podataka
        public bool checkLicence()
        {
            if(getFromDB.GetValueFromDB("select licenca from kase where id_uredaja like '" + userdata.Default.id_uredaja + "'") != "")
            {
                return true;
            }
            return false;
        }

        // Dobivanje licence za kasu
        public string getLicenca()
        {
            return getFromDB.GetValueFromDB("select licenca from kase where id_uredaja like '" + userdata.Default.id_uredaja + "'");
        }

        // Dobivanje datuma od
        public string getDatumOd()
        {
            return getFromDB.GetValueFromDB("select datum_od from kase where id_uredaja like '" + userdata.Default.id_uredaja + "'");
        }

        // Dobivanje datuma do
        public string getDatumDo()
        {
            return getFromDB.GetValueFromDB("select datum_do from kase where id_uredaja like '" + userdata.Default.id_uredaja + "'");
        }

        // Provjera licence offline
        public bool checkLicenceOffline()
        {
            if (userdata.Default.licenca != "")
                return true;
            else
                return false;
        }

        public bool checkDatumOffline()
        {
            if (userdata.Default.licenca_datum_do != "" && userdata.Default.licenca_datum_od != "")
                return true;
            else
                return false;
        }

        // Provjera postoje li podaci o uređaju u bazi
        public bool checkIfDeviceInDB()
        {
            if (getFromDB.GetValueFromDB("select id_uredaja from kase where id_uredaja like '" + userdata.Default.id_uredaja + "'") != "")
                return true;
            else
                return false;
        }


        // Izrada querya za licence
        public void sendLicenceRequest(string[] firma, string[] posl_pr, string[] kasa, string[] userLocationIDs, string[] businessLocationIDs)
        {
            string[] fields_user = {
                "oib_vlasnika",
                "ime_vlasnika",
                "prezime_vlasnika",
                "email_vlasnika",
                "telefon_vlasnika",
                "adresa_vlasnika",
                "sifra_drzave",
                "sifra_zupanije",
                "sifra_opcine",
                "sifra_mjesta",
            };

            string[] fields_business = {
                "oib_vlasnika",
                "naziv_poslovnog_pr",
                "adresa",
                "fina_certifikat",
                "radno_vrijeme",
                "email_pp",
                "telefon_pp",
                "sifra_drzave",
                "sifra_zupanije",
                "sifra_opcine",
                "sifra_mjesta",
            };

            string[] fields_device = {
                "sifra_pp",
                "oib_vlasnika",
                "id_uredaja",
                "naziv_kase",
                "sifra_admin",
                "licenca",
                "datum_od",
                "datum_do",
            };

            // Spajanje polja (dodavanje id-a lokacije)
            string[] tmpUserData = firma.Concat(userLocationIDs).ToArray();
            string[] tmpBusinessData = posl_pr.Concat(businessLocationIDs).ToArray();
            
            // Generiranje Querya za unos
            string firmaQuery = queryController.buildInsertQuery("fiskalvictory", "firme", fields_user, tmpUserData);
            string businessQuery = queryController.buildInsertQuery("fiskalvictory", "poslovni_prostori", fields_business, tmpBusinessData);

            // Izvršavanje querya
            insertDB.insert(firmaQuery);
            insertDB.insert(businessQuery);

            // Vraćanje sifre poslovnog prostora za unos kase
            string businessID = getFromDB.GetValueFromDB("select sifra_poslovnog_pr from poslovni_prostori where oib_vlasnika like '" + firma[0] + "' order by sifra_poslovnog_pr desc LIMIT 1;");
            // Spajanje polja (dodavanje praznog polja i dva datuma)
            string[] tmpDeviceData = new[] { businessID }.Concat(kasa).ToArray().Concat(new[] { "", DateTime.Today.ToString("yyyy-MM-dd"), DateTime.Today.AddMonths(1).ToString("yyyy-MM-dd") }).ToArray(); ;
            // Generiranje query-a
            string deviceQuery = queryController.buildInsertQuery("fiskalvictory", "kase", fields_device, tmpDeviceData);
            
            // Izvršavanje quarya
            insertDB.insert(deviceQuery);
        }
    }
}
