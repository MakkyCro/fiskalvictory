﻿using FiskalVictory.DataAccess;
using FiskalVictory.MVVM.Models;
using Microsoft.EntityFrameworkCore;
using PdfSharpCore.Drawing;
using PdfSharpCore.Pdf;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection.Metadata;
using System.Runtime.InteropServices.ComTypes;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media.Media3D;

namespace FiskalVictory.Core
{
    public class PDF
    {

        public void ispisRacunaPDF(RacunContext c, Racun r)
        {
            XFont f_naslov = new XFont("Consolas", 16, XFontStyle.Regular);
            XFont f_tekst = new XFont("Consolas", 10, XFontStyle.Regular);

            PdfDocument racun = new PdfDocument();

            PdfPage stranica = racun.AddPage();
            stranica.Width = 302;
            stranica.Height = 1000;

            XGraphics gfx = XGraphics.FromPdfPage(stranica);

            int yPos = 10;

            gfx.DrawString("***************************************************", f_tekst, XBrushes.Black, new XRect(0, yPos, stranica.Width, stranica.Height), XStringFormats.TopCenter);
            yPos += 15;
            string naziv_pp = userdata.Default.naziv_pp;
            gfx.DrawString(naziv_pp, f_naslov, XBrushes.Black, new XRect(0, yPos, stranica.Width, stranica.Height), XStringFormats.TopCenter);
            yPos += 25;

            string adresa_pp = userdata.Default.mjesto_pp + ", " + userdata.Default.adresa_pp;
            gfx.DrawString(adresa_pp, f_tekst, XBrushes.Black, new XRect(0, yPos, stranica.Width, stranica.Height), XStringFormats.TopCenter);
            yPos += 10;
            adresa_pp = userdata.Default.opcina_pp + ", " + userdata.Default.drzava_pp;
            gfx.DrawString(adresa_pp, f_tekst, XBrushes.Black, new XRect(0, yPos, stranica.Width, stranica.Height), XStringFormats.TopCenter);
            yPos += 20;
            gfx.DrawString("OIB: " + userdata.Default.oib_vlasnika, f_tekst, XBrushes.Black, new XRect(0, yPos, stranica.Width, stranica.Height), XStringFormats.TopCenter);
            yPos += 15;
            gfx.DrawString("***************************************************", f_tekst, XBrushes.Black, new XRect(0, yPos, stranica.Width, stranica.Height), XStringFormats.TopCenter);
            yPos += 15;
            string datum_i_vrijeme = r.datum_izdavanja + " " + r.vrijeme_izdavanja;
            gfx.DrawString(datum_i_vrijeme, f_tekst, XBrushes.Black, new XRect(10, yPos, stranica.Width, stranica.Height), XStringFormats.TopLeft);
            string operater = c.Radnici.Where(b => b.oib_radnika == r.oibRadnika).Single().ime_radnika + " " + c.Radnici.Where(b => b.oib_radnika == r.oibRadnika).Single().prezime_radnika;
            gfx.DrawString("Operater: " + operater, f_tekst, XBrushes.Black, new XRect(0, yPos, stranica.Width - 10, stranica.Height), XStringFormats.TopRight);
            yPos += 15;

            string racunID = r.sifra_racuna;
            gfx.DrawString("Račun br.: " + racunID, f_naslov, XBrushes.Black, new XRect(10, yPos, stranica.Width, stranica.Height), XStringFormats.TopLeft);
            yPos += 25;

            gfx.DrawString("Naziv", f_tekst, XBrushes.Black, new XRect(10, yPos, stranica.Width, stranica.Height), XStringFormats.TopLeft);
            gfx.DrawString("Iznos", f_tekst, XBrushes.Black, new XRect(0, yPos, stranica.Width - 10, stranica.Height), XStringFormats.TopRight);
            gfx.DrawString("Kol.", f_tekst, XBrushes.Black, new XRect(0, yPos, stranica.Width - 55, stranica.Height), XStringFormats.TopRight);
            gfx.DrawString("Cijena", f_tekst, XBrushes.Black, new XRect(0, yPos, stranica.Width - 100, stranica.Height), XStringFormats.TopRight);
            yPos += 10;
            gfx.DrawString("----------------------------------------------------", f_tekst, XBrushes.Black, new XRect(0, yPos, stranica.Width, stranica.Height), XStringFormats.TopCenter);

            yPos += 15;

            var stavke = c.Stavke.Local.Where(b => b.sifra_racuna == r.sifra_racuna);
            foreach (Stavka s in stavke)
            {
                Artikl a = c.Artikli.Where(b => b.sifra_artikla == s.artiklID).Single();
                gfx.DrawString(a.ime_artikla, f_tekst, XBrushes.Black, new XRect(10, yPos, stranica.Width, stranica.Height), XStringFormats.TopLeft);
                gfx.DrawString(s.cijena_stavke.ToString() + " kn", f_tekst, XBrushes.Black, new XRect(0, yPos, stranica.Width - 10, stranica.Height), XStringFormats.TopRight);
                gfx.DrawString(s.kolicina.ToString() + "x", f_tekst, XBrushes.Black, new XRect(0, yPos, stranica.Width - 55, stranica.Height), XStringFormats.TopRight);
                gfx.DrawString(a.osnovica.ToString() + " kn", f_tekst, XBrushes.Black, new XRect(0, yPos, stranica.Width - 100, stranica.Height), XStringFormats.TopRight);
                yPos += 15;
            }

            gfx.DrawString("----------------------------------------------------", f_tekst, XBrushes.Black, new XRect(0, yPos, stranica.Width, stranica.Height), XStringFormats.TopCenter);
            yPos += 12;
            gfx.DrawString("Ukupno:", f_naslov, XBrushes.Black, new XRect(10, yPos, stranica.Width, stranica.Height), XStringFormats.TopLeft);
            gfx.DrawString(r.ukupna_cijena + " kn", f_naslov, XBrushes.Black, new XRect(0, yPos, stranica.Width - 10, stranica.Height), XStringFormats.TopRight);

            yPos += 25;
            gfx.DrawString("Vrsta poreza", f_tekst, XBrushes.Black, new XRect(10, yPos, stranica.Width, stranica.Height), XStringFormats.TopLeft);
            gfx.DrawString("Iznos", f_tekst, XBrushes.Black, new XRect(0, yPos, stranica.Width - 10, stranica.Height), XStringFormats.TopRight);
            gfx.DrawString("Osnovica", f_tekst, XBrushes.Black, new XRect(0, yPos, stranica.Width - 75, stranica.Height), XStringFormats.TopRight);
            yPos += 15;

            Dictionary<string, double> porezi = new Dictionary<string, double>();

            foreach (Stavka s in stavke)
            {
                Artikl a = c.Artikli.Where(b => b.sifra_artikla == s.artiklID).Single();
                Kategorija k = c.Kategorija.Where(b => b.sifra_kategorije == a.kategorijaID).Single();

                if (!porezi.ContainsKey("PDV " + k.PDV) && k.PDV != 0)
                {
                    porezi["PDV " + k.PDV] = 0;
                }
                if (!porezi.ContainsKey("PnP " + k.PNP) && k.PNP != 0)
                {
                    porezi["PnP " + k.PNP] = 0;
                }

                if (k.PDV != 0)
                {
                    porezi["PDV " + k.PDV] += Math.Round((a.osnovica * s.kolicina / (1.0 + (k.PDV + k.PNP) / 100.0)), 2);
                }

                if (k.PNP != 0)
                {
                    porezi["PnP " + k.PNP] += Math.Round((a.osnovica * s.kolicina / (1.0 + (k.PDV + k.PNP) / 100.0)), 2);
                }

            }

            foreach (var p in porezi)
            {
                gfx.DrawString(p.Key.ToString() + "%", f_tekst, XBrushes.Black, new XRect(10, yPos, stranica.Width, stranica.Height), XStringFormats.TopLeft);
                gfx.DrawString(Math.Round(double.Parse(p.Key.Split(" ")[1]) * (p.Value / 100.0), 2) + " kn", f_tekst, XBrushes.Black, new XRect(0, yPos, stranica.Width - 10, stranica.Height), XStringFormats.TopRight);
                gfx.DrawString(Math.Round(p.Value, 2) + " kn", f_tekst, XBrushes.Black, new XRect(0, yPos, stranica.Width - 75, stranica.Height), XStringFormats.TopRight);
                yPos += 15;
            }

            yPos += 5;
            gfx.DrawString("----------------------------------------------------", f_tekst, XBrushes.Black, new XRect(0, yPos, stranica.Width, stranica.Height), XStringFormats.TopCenter);
            yPos += 15;
            gfx.DrawString("Zaštitni ključ izdavatelja računa:", f_tekst, XBrushes.Black, new XRect(10, yPos, stranica.Width, stranica.Height), XStringFormats.TopLeft);
            yPos += 15;
            gfx.DrawString(r.ZK.Substring(0, 45), f_tekst, XBrushes.Black, new XRect(10, yPos, stranica.Width - 10, stranica.Height), XStringFormats.TopLeft);
            yPos += 15;
            gfx.DrawString("Jedinstveni identifikator računa:", f_tekst, XBrushes.Black, new XRect(10, yPos, stranica.Width, stranica.Height), XStringFormats.TopLeft);
            yPos += 15;
            gfx.DrawString(r.JIR.Substring(0, 45), f_tekst, XBrushes.Black, new XRect(10, yPos, stranica.Width - 10, stranica.Height), XStringFormats.TopLeft);
            yPos += 15;
            gfx.DrawString("----------------------------------------------------", f_tekst, XBrushes.Black, new XRect(0, yPos, stranica.Width, stranica.Height), XStringFormats.TopCenter);
            yPos += 15;
            gfx.DrawString("PDV JE URAČUNAT U CIJENU", f_tekst, XBrushes.Black, new XRect(0, yPos, stranica.Width, stranica.Height), XStringFormats.TopCenter);
            yPos += 15;
            gfx.DrawString("Hvala i doviđenja!", f_tekst, XBrushes.Black, new XRect(0, yPos, stranica.Width, stranica.Height), XStringFormats.TopCenter);
            yPos += 15;
            gfx.DrawString("----------------------------------------------------", f_tekst, XBrushes.Black, new XRect(0, yPos, stranica.Width, stranica.Height), XStringFormats.TopCenter);
            yPos += 15;
            gfx.DrawString("WiFi: pitaj konobara", f_tekst, XBrushes.Black, new XRect(0, yPos, stranica.Width, stranica.Height), XStringFormats.TopCenter);
            yPos += 15;
            gfx.DrawString("FiskalVictory", f_tekst, XBrushes.Black, new XRect(0, yPos, stranica.Width, stranica.Height), XStringFormats.TopCenter);
            string sifra = r.sifra_racuna.Replace("/", "_");

            stranica.CropBox = new PdfRectangle(new XPoint(0, stranica.Height - yPos - 20),
                                new XSize(stranica.Width, 1000));

            racun.Save("Racun_" + sifra + ".pdf");
        }
    }
}
