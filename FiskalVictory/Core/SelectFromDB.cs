﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Windows;
using MySqlConnector;
using System.Configuration;

namespace FiskalVictory.Core
{
    public sealed class SelectFromDB
    {
        public static readonly SelectFromDB getFromDataBase = new SelectFromDB();
        private SelectFromDB() { }

        public static SelectFromDB GetFromDataBase
        {
            get { return getFromDataBase; }
        }

        string connectionDB = ConfigurationManager.ConnectionStrings["FV_DB"].ToString();

        public DataTable selectAll(string comm)
        {
            MySqlConnection connection = null;
            MySqlDataReader reader = null;
            try
            {
                connection = new MySqlConnection(connectionDB);
                connection.Open();

                MySqlDataAdapter dataAdapter = new MySqlDataAdapter();
                dataAdapter.SelectCommand = new MySqlCommand(comm, connection);
                DataTable table = new DataTable();
                dataAdapter.Fill(table);
                return table;
            }
            finally
            {
                if (reader != null)
                    reader.Close();
                if (connection != null)
                    connection.Close();
            }
        }

        // Zatraži vrijednost iz baze podataka na AWS-u
        public string GetValueFromDB(string strQuery)
        {
            string strData = "";

            try
            {
                // Provjera sadrži li query
                if (string.IsNullOrEmpty(strQuery) == true)
                    return string.Empty;

                // Napravi novu vezu
                using (var mysqlconnection = new MySqlConnection(connectionDB))
                {
                    // Početak veze
                    mysqlconnection.Open();

                    // Izrada i izvršavanje naredbe
                    using (MySqlCommand cmd = mysqlconnection.CreateCommand())
                    {
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandTimeout = 300;
                        cmd.CommandText = strQuery;

                        object objValue = cmd.ExecuteScalar();

                        // Ukoliko je prazan rezultat
                        if (objValue == null)
                        {
                            cmd.Dispose();
                            return string.Empty;
                        }
                        // Ukoliko sadrži rezultat
                        else
                        {
                            strData = cmd.ExecuteScalar().ToString();

                            cmd.Dispose();
                        }

                        // Zatvaranje veze
                        mysqlconnection.Close();

                        if (strData == null)
                            return string.Empty;
                        else
                            return strData;
                    }
                }
            }
            catch (MySqlException ex)
            {
                MessageBox.Show(ex.ToString());
                return string.Empty;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                return string.Empty;
            }
        }
    }
}
