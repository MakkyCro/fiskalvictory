﻿using FiskalVictory.DataAccess;
using Microsoft.EntityFrameworkCore;
using System.Windows;

namespace FiskalVictory
{
    public partial class App : Application
    {
        public App()
        {
            using (RacunContext dbContext = new RacunContext())
            {
                dbContext.Database.Migrate();
            }
        }
    }
}
