﻿using FiskalVictory.DataAccess;
using FiskalVictory.MVVM.Models;
using FiskalVictory.MVVM.Views;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Channels;
using System.Windows;
using System.Windows.Controls;

namespace FiskalVictory.MVVM.ViewModels
{
    public class RacuniViewModel
    {
        // Pristupanje bazi podataka
        RacunContext c = new RacunContext();
        public void Load(DataGrid dg)
        {
            // Čitanje svih redaka 
            c.Racuni.Load();

            dg.ItemsSource = c.Racuni.Local.ToObservableCollection();

            dg.Columns.RemoveAt(8);
            dg.Columns.RemoveAt(9);

            dg.Columns[0].Header = "Šifra računa";
            dg.Columns[1].Header = "Datum izdavanja";
            dg.Columns[2].Header = "Vrijeme izdavanja";
            dg.Columns[3].Header = "Ukupna cijena";
        }

        public void FiskalizirajSve(DataGrid dg)
        {
            var racuni = c.Racuni;

            foreach (Racun r in racuni)
            {
                if (r.fiskaliziran == false)
                {
                    r.fiskaliziran = true;
                    r.JIR = Sha256Hash(r.ukupna_cijena + r.vrijeme_izdavanja + r.datum_izdavanja + r.oibRadnika + r.sifra_racuna);
                    c.Racuni.Update(r);
                }
            }
            c.SaveChanges();
            dg.ItemsSource = null;
            dg.ItemsSource = c.Racuni.Local.ToObservableCollection();

            dg.Columns.RemoveAt(8);
            dg.Columns.RemoveAt(9);

            dg.Columns[0].Header = "Šifra računa";
            dg.Columns[1].Header = "Datum izdavanja";
            dg.Columns[2].Header = "Vrijeme izdavanja";
            dg.Columns[3].Header = "Ukupna cijena";
        }

        static string Sha256Hash(string data)
        {
            // Create a SHA256   
            using (SHA256 sha256Hash = SHA256.Create())
            {
                // ComputeHash - returns byte array  
                byte[] bytes = sha256Hash.ComputeHash(Encoding.UTF8.GetBytes(data));

                // Convert byte array to a string   
                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < bytes.Length; i++)
                {
                    builder.Append(bytes[i].ToString("x2"));
                }
                return builder.ToString();
            }
        }
    }
}
