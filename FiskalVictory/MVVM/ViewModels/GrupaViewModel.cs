﻿using FiskalVictory.DataAccess;
using FiskalVictory.MVVM.Models;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Channels;
using System.Windows;
using System.Windows.Controls;

namespace FiskalVictory.MVVM.ViewModels
{
    public class GrupaViewModel
    {
        // Pristupanje bazi podataka
        RacunContext c = new RacunContext();
        public void Load(DataGrid dg)
        {
            // Čitanje svih redaka 
            c.Grupa.Load();

            dg.ItemsSource = c.Grupa.Local.ToObservableCollection();

            dg.Columns.RemoveAt(2);

            dg.Columns[0].Header = "Šifra grupe";
            dg.Columns[1].Header = "Naziv grupe";
        }

        // Ukoliko korisnik pritisne na neki od redaka, tada ažuriramo podatke
        public void ItemSelected(DataGrid dg, TextBox sifra_grupe, TextBox naziv_grupe, Button Save, Button Cancel, Button Remove)
        {
            // Postavi tekstove
            try
            {
                // Uzmi selektirani redak
                Grupa g = (Grupa)dg.SelectedItem;

                if (g != null)
                {
                    sifra_grupe.Text = g.sifra_grupe.ToString();
                    naziv_grupe.Text = g.naziv_grupe;
                    Save.IsEnabled = true;
                    Cancel.IsEnabled = true;
                    Remove.IsEnabled = true;
                    sifra_grupe.IsEnabled = true;
                    naziv_grupe.IsEnabled = true;
                }
            }
            catch
            {
                Unselect(dg, sifra_grupe, naziv_grupe, Save, Cancel, Remove);
            }
        }

        public void Unselect(DataGrid dg, TextBox sifra_grupe, TextBox naziv_grupe, Button Save, Button Cancel, Button Remove)
        {
            dg.SelectedItem = null;
            sifra_grupe.Text = "";
            naziv_grupe.Text = "";
            Save.IsEnabled = false;
            Cancel.IsEnabled = false;
            Remove.IsEnabled = false;
            sifra_grupe.IsEnabled = false;
            naziv_grupe.IsEnabled = false;
        }

        // Brisanje retka 
        public void Remove(DataGrid dg, TextBox sifra_grupe, TextBox naziv_grupe, Button Save, Button Cancel, Button Remove)
        {
            if (MessageBox.Show("Jeste li sigurni da želite obrisati odabranu grupu?", "Brisanje grupe", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
            {
                // Uzmi selektirani redak
                Grupa g = (Grupa)dg.SelectedItem;
                Unselect(dg, sifra_grupe, naziv_grupe, Save, Cancel, Remove);
                c.Grupa.Remove(g);
                c.SaveChanges();
            }
        }

        // Pritiskom na gumb za novi proizvod počistimo vrijednosti u elementima
        public void New(DataGrid dg, TextBox sifra_grupe, TextBox naziv_grupe, Button Save, Button Cancel)
        {
            sifra_grupe.Text = "";
            naziv_grupe.Text = "";
            dg.SelectedItem = null;
            Save.IsEnabled = true;
            Cancel.IsEnabled = true;
            sifra_grupe.IsEnabled = true;
            naziv_grupe.IsEnabled = true;
            sifra_grupe.Focus();
        }

        // Dodavanje novog retka i spremanje
        public void AddAndSaveChanges(DataGrid dg, TextBox sifra_grupe, TextBox naziv_grupe, Button Save, Button Cancel, Button Remove)
        {
            // Provjera praznih textboxeva
            if (sifra_grupe.Text == "" || naziv_grupe.Text == "")
            {
                MessageBox.Show("Trebate ispuniti sva područja kako biste uspješno dodali ili uredili grupu.");
                return;
            }
            try
            {
                // Uzimanje selektiranog retka kako bismo provjerili radi li se o uređivanju ili dodavanju novog retka
                int selectedID = 0;
                Grupa selectedItem = (Grupa)dg.SelectedItem;
                if (selectedItem != null)
                {
                    selectedID = selectedItem.sifra_grupe;
                }

                // Definiranje novog unosa
                Grupa item = new Grupa();
                item.sifra_grupe = int.Parse(sifra_grupe.Text);
                item.naziv_grupe = naziv_grupe.Text;

                // Ukoliko je uređivanje, potrebno je provjeriti 
                if (selectedItem != null)
                {
                    if (c.Grupa.Where(b => b.sifra_grupe == selectedID).Single() != null)
                    {
                        c.Grupa.Remove(selectedItem);
                        c.Grupa.Add(item);

                        dg.ItemsSource = null;
                        dg.ItemsSource = c.Grupa.Local.ToObservableCollection();
                        Unselect(dg, sifra_grupe, naziv_grupe,Save, Cancel, Remove);

                        sifra_grupe.Focus();

                        // Uređivanje stupaca

                        dg.Columns.RemoveAt(2);

                        dg.Columns[0].Header = "Šifra grupe";
                        dg.Columns[1].Header = "Naziv grupe";
                    }
                }

                else
                {
                    // Dodavanje u tablicu
                    c.Grupa.Add(item);
                }
                Unselect(dg, sifra_grupe, naziv_grupe, Save, Cancel, Remove);

                Save.IsEnabled = true;
                Cancel.IsEnabled = true;
                sifra_grupe.IsEnabled = true;
                naziv_grupe.IsEnabled = true;
                sifra_grupe.Focus();
                // Spremanje promjena
                c.SaveChanges();
            }
            catch
            {
                MessageBox.Show("Greška prilikom dodavanja ili uređivanja grupe.");
            }
        }

        // Odustajanje od dodavanja novog unosa
        public void CancelNewEntry(DataGrid dg, TextBox sifra_grupe, TextBox naziv_grupe, Button Save, Button Cancel, Button Remove)
        {
            Unselect(dg, sifra_grupe, naziv_grupe, Save, Cancel, Remove);
        }
    }
}
