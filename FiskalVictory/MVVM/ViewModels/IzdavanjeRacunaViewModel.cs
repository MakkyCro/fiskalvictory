﻿using System;
using System.Text;
using System.Windows.Controls;
using System.Windows;
using FiskalVictory.DataAccess;
using FiskalVictory.MVVM.Models;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Security.Cryptography;
using FiskalVictory.Core;

namespace FiskalVictory.MVVM.ViewModels
{

    public class IzdavanjeRacunaViewModel
    {

        RacunContext c = new RacunContext();
        Racun r = new Racun();
        FiskalVictoryWindow main = FiskalVictoryWindow.MainWindowFV;
        WrapPanel wp_Grupe;
        WrapPanel wp_Artikli;
        StackPanel sp_Racun;
        DataGrid dg;
        Style buttonStyle;
        IsConnected network = new IsConnected();
        PDF pdf = new PDF();
        Stavka odabranaStavka;

        Button br;
        Button iz;
        ComboBox np;

        // Učitavanje svih grupa u obliku gumba, dodavanje stilova i clickEventa
        public void LoadedIzdavanje(DataGrid _dg, WrapPanel wpG, WrapPanel wpA, StackPanel spR, ComboBox cb_nacin_placanja, Button _br, Button _iz )
        {
            wp_Grupe = wpG;
            wp_Artikli = wpA;
            sp_Racun = spR;
            dg = _dg;

            np = cb_nacin_placanja;
            br = _br;
            iz = _iz;
            // Lociranje stila za gumb i dodavanje na gumb
            ResourceDictionary res = (ResourceDictionary)Application.LoadComponent(new Uri("Themes/button.xaml", UriKind.Relative));
            buttonStyle = (Style)res["buttonStyle"];
            // Učitavanje radnika
            c.Radnici.Load();
            // Definiranje šifre računa i dodavanje operatera
            int broj_racuna = c.Racuni.Count() + 1;
            r.sifra_racuna = broj_racuna.ToString() + "/" + userdata.Default.sifra_pp + "/" + userdata.Default.sifra_kase;

            //if (main.user != "Administrator")
            r.oibRadnika = c.Radnici.Where(b => b.ime_radnika + " " + b.prezime_radnika == main.user).Single().oib_radnika;
            //else
            //    r.oibRadnika = userdata.Default.oib_vlasnika;

            r.nacinPlacanjaID = c.NacinPlacanja.Where(b => b.naziv_placanja == cb_nacin_placanja.Text).Single().sifra_placanja;

            //c.Racuni.Add(r);
            //c.SaveChanges();

            // Učitavanje grupa 
            c.Grupa.Load();
            var grupe = c.Grupa;
            // Dinamičko alociranje gumbova za grupe
            foreach (var g in grupe)
            {
                Button b = new Button();
                b.Content = new TextBlock { Text = g.naziv_grupe, TextWrapping = TextWrapping.Wrap, TextAlignment = TextAlignment.Center };
                b.Style = buttonStyle;
                b.Height = 100;
                b.Width = 175;
                b.FontSize = 18;
                b.Foreground = System.Windows.Media.Brushes.White;
                b.Background = System.Windows.Media.Brushes.Transparent;
                b.VerticalContentAlignment = VerticalAlignment.Center;
                b.Tag = g.sifra_grupe;
                b.Margin = new Thickness(5, 15, 0, 15);
                // Dodavanje Eventa
                b.Click += new RoutedEventHandler(grupaClicked);
                wp_Grupe.Children.Add(b);
            }
        }

        // Definiranje funkcije što će se dogoditi pritiskom na gumb grupe
        protected void grupaClicked(object sender, EventArgs e)
        {
            // Resetiranje popisa artikala
            wp_Artikli.Children.Clear();
            // Učitavanje artikala određene grupe
            Button button = sender as Button;
            c.Artikli.Load();
            var artikli = c.Artikli.Where(b => b.grupaID == int.Parse(button.Tag.ToString()));
            // Definiranje novih gumbova za artikle i dodavanje funkcija
            foreach (var a in artikli)
            {
                Button b = new Button();
                b.Content = new TextBlock { Text = a.ime_artikla, TextWrapping = TextWrapping.Wrap, TextAlignment = TextAlignment.Center };
                b.Style = buttonStyle;
                b.Height = 100;
                b.Width = 175;
                b.FontSize = 18;
                b.Foreground = System.Windows.Media.Brushes.White;
                b.Background = System.Windows.Media.Brushes.Transparent;
                b.VerticalContentAlignment = VerticalAlignment.Center;
                b.Tag = a.sifra_artikla.ToString();
                b.Margin = new Thickness(5, 15, 0, 15);

                b.Click += new RoutedEventHandler(artiklClicked);
                wp_Artikli.Children.Add(b);
            }
        }

        // Funkcija koja opisuje što će se dogoditi pritiskom na gumb artikla
        protected void artiklClicked(object sender, EventArgs e)
        {
            // Dobivanje šifre artikla
            Button button = sender as Button;
            int _artiklID = int.Parse(button.Tag.ToString());
            // Učitavanje Stavka
            c.Stavke.Load();

            // Provjera sadrži li koja stavka već navedeni artikl ili ne definiranje nove stavke ili ažuriranje postojeće
            Stavka s;
            if (c.Stavke.Local.Where(b => b.sifra_racuna == r.sifra_racuna && b.artiklID == _artiklID).Count() == 0)
                s = new Stavka();
            else
                s = c.Stavke.Local.Where(b => b.sifra_racuna == r.sifra_racuna && b.artiklID == _artiklID).Single();

            // Pristupanje ostalim podacima
            Artikl a = c.Artikli.Where(b => b.sifra_artikla == _artiklID).Single();
            Kategorija k = c.Kategorija.Where(b => b.sifra_kategorije == a.kategorijaID).Single();
            // Definiranje šifre računa te definiranje količine stavke
            s.sifra_racuna = r.sifra_racuna;

            if (c.Stavke.Local.Where(b => b.sifra_racuna == r.sifra_racuna && b.artiklID == _artiklID).Count() == 0)
            {
                s.kolicina = 1;
                s.rb_stavke = c.Stavke.Local.Where(b => b.sifra_racuna == r.sifra_racuna).Count() + 1;
            }
            else
            {
                s.kolicina += 1;
            }

            // Definiranje šifre artikla i cijene stavke u Stavka tablici 
            s.artiklID = a.sifra_artikla;
            s.cijena_stavke = a.osnovica * s.kolicina;

            // Provjera postoji li artikl u nekoj stavci, ukoliko postoji brišemo (zbog ažuriranja)
            if (c.Stavke.Local.Where(b => b.sifra_racuna == r.sifra_racuna && b.artiklID == _artiklID).Count() != 0)
            {
                Stavka stavka = c.Stavke.Local.Where(b => b.sifra_racuna == r.sifra_racuna && b.artiklID == _artiklID).Single();
                c.Stavke.Remove(stavka);
            }
            // Dodavanje stavke
            c.Stavke.Add(s);

            // Ažuriranje DataGrid elementa
            dg.Items.Clear();
            r.ukupna_cijena = 0;
            foreach (Stavka sta in c.Stavke.Local.Where(b => b.sifra_racuna == r.sifra_racuna))
            {
                c.Artikli.Load();
                Artikl art = c.Artikli.Where(b => b.sifra_artikla == sta.artiklID).Single();
                var data = new { RbStavke = sta.rb_stavke, NazivArtikla = art.ime_artikla, Kolicina = sta.kolicina, Osnovica = art.osnovica.ToString(), CijenaStavke = sta.cijena_stavke };
                r.ukupna_cijena += sta.cijena_stavke;
                dg.Items.Add(data);
            }

            sp_Racun.Children.Clear();
            string operater = c.Radnici.Where(b => b.oib_radnika == r.oibRadnika).Single().ime_radnika + " " + c.Radnici.Where(b => b.oib_radnika == r.oibRadnika).Single().prezime_radnika;
            TextBlock tb = new TextBlock();
            tb.Tag = "RACUN";
            tb.Foreground = System.Windows.Media.Brushes.White;
            tb.FontSize = 18;
            tb.Text = "Šifra računa: " + r.sifra_racuna + "\nUkupna cijena: " + r.ukupna_cijena.ToString() + " kn\nOperater: " + operater;
            sp_Racun.Children.Add(tb);

            if (dg.Items.Count > 0)
            {
                br.IsEnabled = true;
                iz.IsEnabled = true;
                np.IsEnabled = true;
            }
        }

        public void loadNacinPlacanja(ComboBox cb)
        {
            c.NacinPlacanja.Load();
            var naciniplacanja = c.NacinPlacanja.Local.ToObservableCollection();
            foreach (NacinPlacanja np in naciniplacanja)
            {
                cb.Items.Add(np.naziv_placanja);
            }
            cb.SelectedIndex = 0;
        }

        public void izdajRacun(DataGrid dg, Button izdavanje, Button brisanje, ComboBox nacin_placanja, StackPanel sp)
        {
            r.datum_izdavanja = DateTime.Now.ToString("dd.MM.yyyy.", new System.Globalization.CultureInfo("hr-HR"));
            r.vrijeme_izdavanja = DateTime.Now.ToString("HH:mm:ss");

            // Definiranje ZK
            r.ZK = Sha256Hash(r.sifra_racuna + r.oibRadnika + r.datum_izdavanja + r.vrijeme_izdavanja + r.ukupna_cijena);

            if (network.IsConnectedToInternet())
            {
                r.fiskaliziran = true;
                r.JIR = Sha256Hash(r.ukupna_cijena + r.vrijeme_izdavanja + r.datum_izdavanja + r.oibRadnika + r.sifra_racuna);
            }
            else
            {
                r.fiskaliziran = false;
                r.JIR = "";
            }


            c.Racuni.Add(r);

            pdf.ispisRacunaPDF(c, r);

            c.SaveChanges();

            izdavanje.IsEnabled = false;
            brisanje.IsEnabled = false;
            //kolicina.IsEnabled = false;
            nacin_placanja.IsEnabled = false;
            dg.Items.Clear();
            sp.Children.Clear();
        }

        public void odaberiStavku(DataGrid dg, Button izdavanje, Button brisanje,ComboBox nacin_placanja)
        {
            if (dg.Items.Count == 0)
            {
                izdavanje.IsEnabled = false;
                brisanje.IsEnabled = false;
                //kolicina.IsEnabled = false;
                nacin_placanja.IsEnabled = false;
                odabranaStavka = null;
            }
            else
            {
                izdavanje.IsEnabled = true;
                brisanje.IsEnabled = true;
                //kolicina.IsEnabled = true;
                nacin_placanja.IsEnabled = true;
                var odabran = dg.SelectedItem;
                string[] s = odabran.ToString().Split(", ");
                string id = s[0].Last().ToString();
                int ID_stavke = Convert.ToInt32(id);
                odabranaStavka = c.Stavke.Local.Where(b => b.sifra_racuna == r.sifra_racuna && b.rb_stavke == ID_stavke).Single();
                odabran = null;
            }
        }

        public void obrisiStavku(DataGrid dg)
        {
            if (odabranaStavka != null)
            {
                c.Stavke.Local.Remove(odabranaStavka);


                //int index = 1;
                dg.Items.Clear();
                r.ukupna_cijena = 0;
                foreach (Stavka sta in c.Stavke.Local.Where(b => b.sifra_racuna == r.sifra_racuna))
                {
                    //sta.rb_stavke = index;
                    //c.Stavke.Update(sta);
                    c.Artikli.Load();
                    Artikl art = c.Artikli.Where(b => b.sifra_artikla == sta.artiklID).Single();
                    var data = new { RbStavke = sta.rb_stavke, NazivArtikla = art.ime_artikla, Kolicina = sta.kolicina, Osnovica = art.osnovica.ToString(), CijenaStavke = sta.cijena_stavke };
                    r.ukupna_cijena += sta.cijena_stavke;
                    dg.Items.Add(data);
                    //index += 1;
                }

                sp_Racun.Children.Clear();
                string operater = c.Radnici.Where(b => b.oib_radnika == r.oibRadnika).Single().ime_radnika + " " + c.Radnici.Where(b => b.oib_radnika == r.oibRadnika).Single().prezime_radnika;
                TextBlock tb = new TextBlock();
                tb.Tag = "RACUN";
                tb.Foreground = System.Windows.Media.Brushes.White;
                tb.FontSize = 18;
                tb.Text = "Šifra računa: " + r.sifra_racuna + "\nUkupna cijena: " + r.ukupna_cijena.ToString() + " kn\nOperater: " + operater;
                sp_Racun.Children.Add(tb);
            }
        }

        //public void smanjiKolicinu(DataGrid dg)
        //{
        //    if (odabranaStavka != null)
        //    {
        //        if (odabranaStavka.kolicina > 1)
        //        {
        //            odabranaStavka.kolicina -= 1;
        //            c.Stavke.Update(odabranaStavka);
        //        }
        //        else
        //        {
        //            obrisiStavku(dg);
        //        }
        //    }
        //}


        static string Sha256Hash(string data)
        {
            // Create a SHA256   
            using (SHA256 sha256Hash = SHA256.Create())
            {
                // ComputeHash - returns byte array  
                byte[] bytes = sha256Hash.ComputeHash(Encoding.UTF8.GetBytes(data));

                // Convert byte array to a string   
                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < bytes.Length; i++)
                {
                    builder.Append(bytes[i].ToString("x2"));
                }
                return builder.ToString();
            }
        }
    }
}
