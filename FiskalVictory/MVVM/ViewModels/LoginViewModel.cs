﻿using FiskalVictory.Core;
using FiskalVictory.DataAccess;
using FiskalVictory.MVVM.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;

namespace FiskalVictory.MVVM.ViewModels
{
    public class LoginViewModel
    {

        public void CheckForLogin(PasswordBox tb_lozinka)
        {
            FiskalVictoryWindow mainwindow = FiskalVictoryWindow.MainWindowFV;
            Licence licenca = Licence.Lic;
            SelectFromDB getFromDB = SelectFromDB.GetFromDataBase;

            if (licenca.checkIfDeviceInDB() && getFromDB.GetValueFromDB("select licenca from kase where id_uredaja like '" + userdata.Default.id_uredaja + "'") == "")
            {
                MessageBox.Show("Licenca za Vaš uređaj još nije aktivna. ID uređaja: " + userdata.Default.id_uredaja);
            }
            else
            {
                RacunContext c = new RacunContext();
                if (userdata.Default.sifra_admina == tb_lozinka.Password.ToString())
                {
                    Radnik r = c.Radnici.Where(b => b.lozinka_radnika == tb_lozinka.Password.ToString()).Single();
                    mainwindow.user = r.ime_radnika + " " + r.prezime_radnika;
                    mainwindow.LoginPressed();
                }
                else
                {
                    c.Radnici.Load();
                    try
                    {
                        var radnik = c.Radnici.Where(b => b.lozinka_radnika == tb_lozinka.Password.ToString()).Single();
                        if (radnik != null)
                        {
                            mainwindow.user = radnik.ime_radnika + " " + radnik.prezime_radnika;
                            mainwindow.LoginPressed();
                        }
                    }
                    catch
                    {
                        MessageBox.Show("Ne postoji korisnik s tom lozinkom.");
                    }
                }
                c.Dispose();
            }
        }
    }
}
