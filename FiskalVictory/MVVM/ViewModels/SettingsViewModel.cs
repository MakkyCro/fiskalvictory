﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Text;

namespace FiskalVictory.MVVM.ViewModels
{
    public class SettingsViewModel
    {

        public void archiveLocally()
        {
            //var zipFile = @"C:\data\myzip.zip";
            //var files = Directory.GetFiles(@"c:\data");
            string path = System.Reflection.Assembly.GetExecutingAssembly().Location.Replace("FiskalVictory.dll", ""); 
            string zipFile = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\FiskalVictory\\podaci_" + DateTime.Now.ToString("dd.MM.yyyy.hh.mm.ss") + ".zip";

            string[] files = Directory.GetFiles(path);

            if (!Directory.Exists(System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\FiskalVictory"))
                Directory.CreateDirectory(System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\FiskalVictory");

            using (var archive = ZipFile.Open(zipFile, ZipArchiveMode.Create))
            {
                foreach (var fPath in files)
                {
                    if(Path.GetFileName(fPath) == "podaci.db")
                    {
                        archive.CreateEntryFromFile(fPath, Path.GetFileName(fPath));
                    }
                }
            }
        }
    }
}
