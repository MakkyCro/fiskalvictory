﻿using FiskalVictory.Core;
using System.Windows.Input;

namespace FiskalVictory.MVVM.ViewModels
{
    class MainViewModel : ObservableObject
    {
        public RelayCommand HomeVMCommand { get; set; }
        public RelayCommand RegisterUserVMCommand { get; set; }
        public RelayCommand LoginVMCommand { get; set; }
        public HomeViewModel HomeVM { get; set; }
        public RegisterUserViewModel RegisterUserVM { get; set; }
        public LoginViewModel LoginVM { get; set; }

        private object _currentView;

        public object CurrentView
        {
            get
            {
                return _currentView;
            }
            set
            {
                _currentView = value;
                OnPropertyChanged();
            }
        }

        public MainViewModel()
        {
            RegisterUserVM = new RegisterUserViewModel();
            LoginVM = new LoginViewModel();
            HomeVM = new HomeViewModel();
            
            //CurrentView = HomeVM;

            HomeVMCommand = new RelayCommand(o =>
            {
                CurrentView = HomeVM;
            });
            RegisterUserVMCommand = new RelayCommand(o =>
            {
                CurrentView = RegisterUserVM;
            });
            LoginVMCommand = new RelayCommand(o =>
            {
                CurrentView = LoginVM;
            });
        }
    }
}
