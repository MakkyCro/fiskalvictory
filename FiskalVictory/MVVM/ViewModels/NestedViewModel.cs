﻿using FiskalVictory.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace FiskalVictory.MVVM.ViewModels
{
    class NestedViewModel : ObservableObject
    {
        public RelayCommand SpeedDialVMCommand { get; set; }
        public RelayCommand ProductsVMCommand { get; set; }
        public RelayCommand WorkersVMCommand { get; set; }
        public RelayCommand SettingsVMCommand { get; set; }
        public RelayCommand JMVMCommand { get; set; }
        public RelayCommand grupaVMCommand { get; set; }
        public RelayCommand kategorijaVMCommand { get; set; }
        public RelayCommand nacinPlacanjaVMCommand { get; set; }
        public RelayCommand normativiVMCommand { get; set; }
        public RelayCommand racuniVMCommand { get; set; }
        public RelayCommand izdavanjeVMCommand { get; set; }

        public SpeedDialViewModel SpeedDialVM { get; set; }
        public ProductsViewModel ProductsVM { get; set; }
        public WorkersViewModel WorkersVM { get; set; }
        public SettingsViewModel SettingsVM { get; set; }
        public JMViewModel JMVM { get; set; }
        public GrupaViewModel grupaVM { get; set; }
        public KategorijaViewModel kategorijaVM { get; set; }
        public NacinPlacanjaViewModel nacinPlacanjaVM { get; set; }
        public NormativiViewModel normativiVM { get; set; }
        public RacuniViewModel racuniVM { get; set; }
        public IzdavanjeRacunaViewModel izdavanjeVM { get; set; }


        private object _currentNestedView;

        public object CurrentNestedView
        {
            get
            {
                return _currentNestedView;
            }
            set
            {
                _currentNestedView = value;
                OnPropertyChanged();
            }
        }
        public NestedViewModel()
        {
            SpeedDialVM = new SpeedDialViewModel();
            ProductsVM = new ProductsViewModel();
            WorkersVM = new WorkersViewModel();
            SettingsVM = new SettingsViewModel();
            JMVM = new JMViewModel();
            grupaVM = new GrupaViewModel();
            kategorijaVM = new KategorijaViewModel();
            nacinPlacanjaVM = new NacinPlacanjaViewModel();
            normativiVM = new NormativiViewModel();
            racuniVM = new RacuniViewModel();
            izdavanjeVM = new IzdavanjeRacunaViewModel();

            CurrentNestedView = SpeedDialVM;

            SpeedDialVMCommand = new RelayCommand(o =>
            {
                CurrentNestedView = SpeedDialVM;
            });

            ProductsVMCommand = new RelayCommand(o =>
            {
                CurrentNestedView = ProductsVM;
            });

            WorkersVMCommand = new RelayCommand(o =>
            {
                CurrentNestedView = WorkersVM;
            });

            SettingsVMCommand = new RelayCommand(o =>
            {
                CurrentNestedView = SettingsVM;
            });

            JMVMCommand = new RelayCommand(o =>
            {
                CurrentNestedView = JMVM;
            });

            grupaVMCommand = new RelayCommand(o =>
            {
                CurrentNestedView = grupaVM;
            });

            kategorijaVMCommand = new RelayCommand(o =>
            {
                CurrentNestedView = kategorijaVM;
            });

            nacinPlacanjaVMCommand = new RelayCommand(o =>
            {
                CurrentNestedView = nacinPlacanjaVM;
            });

            normativiVMCommand = new RelayCommand(o =>
            {
                CurrentNestedView = normativiVM;
            });

            racuniVMCommand = new RelayCommand(o =>
            {
                CurrentNestedView = racuniVM;
            });

            izdavanjeVMCommand = new RelayCommand(o =>
            {
                CurrentNestedView = izdavanjeVM;
            });
        }
    }
}
