﻿using FiskalVictory.DataAccess;
using FiskalVictory.MVVM.Models;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Channels;
using System.Windows;
using System.Windows.Controls;

namespace FiskalVictory.MVVM.ViewModels
{
    public class JMViewModel
    {
        // Pristupanje bazi podataka
        RacunContext c = new RacunContext();
        public void LoadJM(DataGrid dg)
        {
            // Čitanje svih redaka jedinica mjera
            c.Jed_mj.Load();

            dg.ItemsSource = c.Jed_mj.Local.ToObservableCollection();

            dg.Columns.RemoveAt(2);

            dg.Columns[0].Header = "Šifra jedinice mjere";
            dg.Columns[1].Header = "Naziv jedinice mjere";
        }

        // Ukoliko korisnik pritisne na neki od redaka, tada ažuriramo podatke
        public void ItemSelected(DataGrid dg, TextBox sifra_JM, TextBox naziv_JM, Button Save, Button Cancel, Button Remove)
        {
            // Postavi tekstove
            try
            {
                // Uzmi selektirani redak
                JedinicaMjere jm = (JedinicaMjere)dg.SelectedItem;

                if (jm != null)
                {
                    sifra_JM.Text = jm.sifra_JM.ToString();
                    naziv_JM.Text = jm.naziv_JM;
                    Save.IsEnabled = true;
                    Cancel.IsEnabled = true;
                    Remove.IsEnabled = true;
                    sifra_JM.IsEnabled = true;
                    naziv_JM.IsEnabled = true;
                }
            }
            catch
            {
                Unselect(dg, sifra_JM, naziv_JM, Save, Cancel, Remove);
            }
        }

        public void Unselect(DataGrid dg, TextBox sifra_JM, TextBox naziv_JM, Button Save, Button Cancel, Button Remove)
        {
            dg.SelectedItem = null;
            sifra_JM.Text = "";
            naziv_JM.Text = "";
            Save.IsEnabled = false;
            Cancel.IsEnabled = false;
            Remove.IsEnabled = false;
            sifra_JM.IsEnabled = false;
            naziv_JM.IsEnabled = false;
        }

        // Brisanje retka 
        public void Remove(DataGrid dg, TextBox sifra_JM, TextBox naziv_JM, Button Save, Button Cancel, Button Remove)
        {
            if (MessageBox.Show("Jeste li sigurni da želite obrisati odabranu jedinicu mjere?", "Brisanje jedinice mjere", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
            {
                // Uzmi selektirani redak
                JedinicaMjere jm = (JedinicaMjere)dg.SelectedItem;
                Unselect(dg, sifra_JM, naziv_JM, Save, Cancel, Remove);
                c.Jed_mj.Remove(jm);
                c.SaveChanges();
            }
        }

        // Pritiskom na gumb za novi proizvod počistimo vrijednosti u elementima
        public void New(DataGrid dg, TextBox sifra_JM, TextBox naziv_JM, Button Save, Button Cancel)
        {
            sifra_JM.Text = "";
            naziv_JM.Text = "";
            dg.SelectedItem = null;
            Save.IsEnabled = true;
            Cancel.IsEnabled = true;
            sifra_JM.IsEnabled = true;
            naziv_JM.IsEnabled = true;
            sifra_JM.Focus();
        }

        // Dodavanje novog retka i spremanje
        public void AddAndSaveChanges(DataGrid dg, TextBox sifra_JM, TextBox naziv_JM, Button Save, Button Cancel, Button Remove)
        {
            // Provjera praznih textboxeva
            if (sifra_JM.Text == "" || naziv_JM.Text == "")
            {
                MessageBox.Show("Trebate ispuniti sva područja kako biste uspješno dodali ili uredili jedinicu mjere.");
                return;
            }
            try
            {
                // Uzimanje selektiranog retka kako bismo provjerili radi li se o uređivanju ili dodavanju novog retka
                int selectedID = 0;
                JedinicaMjere selectedItem = (JedinicaMjere)dg.SelectedItem;
                if (selectedItem != null)
                {
                    selectedID = selectedItem.sifra_JM;
                }

                // Definiranje novog unosa
                JedinicaMjere item = new JedinicaMjere();
                item.sifra_JM = int.Parse(sifra_JM.Text);
                item.naziv_JM = naziv_JM.Text;

                // Ukoliko je uređivanje, potrebno je provjeriti
                if (selectedItem != null)
                {
                    if (c.Jed_mj.Where(b => b.sifra_JM == selectedID).Single() != null)
                    {
                        c.Jed_mj.Remove(selectedItem);
                        c.Jed_mj.Add(item);

                        dg.ItemsSource = null;
                        dg.ItemsSource = c.Jed_mj.Local.ToObservableCollection();
                        Unselect(dg, sifra_JM, naziv_JM, Save, Cancel, Remove);

                        // Uređivanje stupaca

                        dg.Columns.RemoveAt(2);

                        dg.Columns[0].Header = "Šifra jedinice mjere";
                        dg.Columns[1].Header = "Naziv jedinice mjere";
                    }
                }

                else
                {
                    // Dodavanje u tablicu
                    c.Jed_mj.Add(item);
                }
                Unselect(dg, sifra_JM, naziv_JM, Save, Cancel, Remove);

                Save.IsEnabled = true;
                Cancel.IsEnabled = true;
                sifra_JM.IsEnabled = true;
                naziv_JM.IsEnabled = true;
                sifra_JM.Focus();
                // Spremanje promjena
                c.SaveChanges();
            }
            catch
            {
                MessageBox.Show("Greška prilikom dodavanja ili uređivanja jedinica mjere.");
            }
        }

        // Odustajanje od dodavanja novog unosa
        public void CancelNewEntry(DataGrid dg, TextBox sifra_JM, TextBox naziv_JM, Button Save, Button Cancel, Button Remove)
        {
            Unselect(dg, sifra_JM, naziv_JM, Save, Cancel, Remove);
        }
    }
}
