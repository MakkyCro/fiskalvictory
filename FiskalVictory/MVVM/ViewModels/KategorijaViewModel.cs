﻿using FiskalVictory.DataAccess;
using FiskalVictory.MVVM.Models;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Channels;
using System.Windows;
using System.Windows.Controls;

namespace FiskalVictory.MVVM.ViewModels
{
    public class KategorijaViewModel
    {
        // Pristupanje bazi podataka
        RacunContext c = new RacunContext();
        public void Load(DataGrid dg)
        {
            // Čitanje svih redaka 
            c.Kategorija.Load();

            dg.ItemsSource = c.Kategorija.Local.ToObservableCollection();

            dg.Columns.RemoveAt(5);

            dg.Columns[0].Header = "Šifra kategorije";
            dg.Columns[1].Header = "Naziv kategorije";
            dg.Columns[2].Header = "PDV";
            dg.Columns[3].Header = "PNP";
            dg.Columns[4].Header = "Opis";
        }

        // Ukoliko korisnik pritisne na neki od redaka, tada ažuriramo podatke
        public void ItemSelected(DataGrid dg, TextBox sifra_kategorije, TextBox naziv_kategorije, TextBox PDV, TextBox PNP, TextBox opis, Button Save, Button Cancel, Button Remove)
        {
            // Postavi tekstove
            try
            {
                // Uzmi selektirani redak
                Kategorija k = (Kategorija)dg.SelectedItem;

                if (k != null)
                {
                    sifra_kategorije.Text = k.sifra_kategorije.ToString();
                    naziv_kategorije.Text = k.naziv_kategorije;
                    PDV.Text = k.PDV.ToString();
                    PNP.Text = k.PNP.ToString();
                    opis.Text = k.opis;
                    Save.IsEnabled = true;
                    Cancel.IsEnabled = true;
                    Remove.IsEnabled = true;
                    sifra_kategorije.IsEnabled = true;
                    naziv_kategorije.IsEnabled = true;
                    PDV.IsEnabled = true;
                    PNP.IsEnabled = true;
                    opis.IsEnabled = true;
                }
            }
            catch
            {
                Unselect(dg, sifra_kategorije, naziv_kategorije, PDV, PNP, opis, Save, Cancel, Remove);
            }
        }

        public void Unselect(DataGrid dg, TextBox sifra_kategorije, TextBox naziv_kategorije, TextBox PDV, TextBox PNP, TextBox opis, Button Save, Button Cancel, Button Remove)
        {
            dg.SelectedItem = null;
            sifra_kategorije.Text = "";
            naziv_kategorije.Text = "";
            PDV.Text = "";
            PNP.Text = "";
            opis.Text = "";
            Save.IsEnabled = false;
            Cancel.IsEnabled = false;
            Remove.IsEnabled = false;
            sifra_kategorije.IsEnabled = false;
            naziv_kategorije.IsEnabled = false;
            PDV.IsEnabled = false;
            PNP.IsEnabled = false;
            opis.IsEnabled = false;
        }

        // Brisanje retka 
        public void Remove(DataGrid dg, TextBox sifra_kategorije, TextBox naziv_kategorije, TextBox PDV, TextBox PNP, TextBox opis, Button Save, Button Cancel, Button Remove)
        {
            if (MessageBox.Show("Jeste li sigurni da želite obrisati odabranu kategoriju?", "Brisanje kategorije", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
            {
                // Uzmi selektirani redak
                Kategorija k = (Kategorija)dg.SelectedItem;
                Unselect(dg, sifra_kategorije, naziv_kategorije, PDV, PNP, opis, Save, Cancel, Remove);
                c.Kategorija.Remove(k);
                c.SaveChanges();
            }
        }

        // Pritiskom na gumb za novi proizvod počistimo vrijednosti u elementima
        public void New(DataGrid dg, TextBox sifra_kategorije, TextBox naziv_kategorije, TextBox PDV, TextBox PNP, TextBox opis, Button Save, Button Cancel)
        {
            sifra_kategorije.Text = "";
            naziv_kategorije.Text = "";
            PDV.Text = "";
            PNP.Text = "";
            opis.Text = "";
            dg.SelectedItem = null;
            Save.IsEnabled = true;
            Cancel.IsEnabled = true;
            sifra_kategorije.IsEnabled = true;
            naziv_kategorije.IsEnabled = true;
            PDV.IsEnabled = true;
            PNP.IsEnabled = true;
            opis.IsEnabled = true;
            sifra_kategorije.Focus();
        }

        // Dodavanje novog retka i spremanje
        public void AddAndSaveChanges(DataGrid dg, TextBox sifra_kategorije, TextBox naziv_kategorije, TextBox PDV, TextBox PNP, TextBox opis, Button Save, Button Cancel, Button Remove)
        {
            // Provjera praznih textboxeva
            if (sifra_kategorije.Text == "" || naziv_kategorije.Text == "" || PDV.Text == "" || PNP.Text == "")
            {
                MessageBox.Show("Trebate ispuniti sva područja (opis je opcionalan) kako biste uspješno dodali ili uredili kategoriju.");
                return;
            }
            try
            {
                // Uzimanje selektiranog retka kako bismo provjerili radi li se o uređivanju ili dodavanju novog retka
                int selectedID = 0;
                Kategorija selectedItem = (Kategorija)dg.SelectedItem;
                if (selectedItem != null)
                {
                    selectedID = selectedItem.sifra_kategorije;
                }

                // Definiranje novog unosa
                Kategorija item = new Kategorija();
                item.sifra_kategorije = int.Parse(sifra_kategorije.Text);
                item.naziv_kategorije = naziv_kategorije.Text;
                item.PDV = int.Parse(PDV.Text);
                item.PNP = int.Parse(PNP.Text);
                item.opis = opis.Text;

                // Ukoliko je uređivanje, potrebno je provjeriti
                if (selectedItem != null)
                {
                    if (c.Kategorija.Where(b => b.sifra_kategorije == selectedID).Single() != null)
                    {
                        c.Kategorija.Remove(selectedItem);
                        c.Kategorija.Add(item);

                        dg.ItemsSource = null;
                        dg.ItemsSource = c.Kategorija.Local.ToObservableCollection();
                        Unselect(dg, sifra_kategorije, naziv_kategorije, PDV, PNP, opis, Save, Cancel, Remove);

                        // Uređivanje stupaca
                        dg.Columns.RemoveAt(5);

                        dg.Columns[0].Header = "Šifra kategorije";
                        dg.Columns[1].Header = "Naziv kategorije";
                        dg.Columns[2].Header = "PDV";
                        dg.Columns[3].Header = "PNP";
                        dg.Columns[4].Header = "Opis";
                    }
                }

                else
                {
                    // Dodavanje u tablicu
                    c.Kategorija.Add(item);
                }
                Unselect(dg, sifra_kategorije, naziv_kategorije, PDV, PNP, opis, Save, Cancel, Remove);
                Save.IsEnabled = true;
                Cancel.IsEnabled = true;
                sifra_kategorije.IsEnabled = true;
                naziv_kategorije.IsEnabled = true;
                PDV.IsEnabled = true;
                PNP.IsEnabled = true;
                opis.IsEnabled = true;
                sifra_kategorije.Focus();
                // Spremanje promjena
                c.SaveChanges();
            }
            catch
            {
                MessageBox.Show("Greška prilikom dodavanja ili uređivanja kategorije.");
            }
        }

        // Odustajanje od dodavanja novog unosa
        public void CancelNewEntry(DataGrid dg, TextBox sifra_kategorije, TextBox naziv_kategorije, TextBox PDV, TextBox PNP, TextBox opis, Button Save, Button Cancel, Button Remove)
        {
            Unselect(dg, sifra_kategorije, naziv_kategorije, PDV, PNP, opis, Save, Cancel, Remove);
        }
    }
}
