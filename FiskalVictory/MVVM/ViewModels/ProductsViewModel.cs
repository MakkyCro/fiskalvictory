﻿using FiskalVictory.DataAccess;
using FiskalVictory.MVVM.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

namespace FiskalVictory.MVVM.ViewModels
{
    public class ProductsViewModel
    {
        // Pristupanje bazi podataka
        RacunContext c = new RacunContext();

        // Funkcija za učitavanje artikala - proslijeđujemo DataGrid element
        public void LoadProducts(DataGrid dg, ComboBox JM, ComboBox Grupa, ComboBox Kategorija)
        {
            // Čitanje svih redaka artikala
            c.Artikli.Load();
            // Učitavanje u DataGrid element
            dg.ItemsSource = c.Artikli.Local.ToObservableCollection();
            // Uređivanje stupaca
            dg.Columns.RemoveAt(6);
            dg.Columns.RemoveAt(7);
            dg.Columns.RemoveAt(8);
            dg.Columns.RemoveAt(8);

            dg.Columns[0].Header = "Šifra artikla";
            dg.Columns[1].Header = "Naziv artikla";
            dg.Columns[2].Header = "Osnovica";
            dg.Columns[3].Header = "Putanja do slike";
            dg.Columns[4].Header = "Vidljivost";

            AddValuesToComboboxes(JM, Grupa, Kategorija);
        }

        public void LoadNormativi(DataGrid dg, string sifraArtikla)
        {
            c.Normativi.Load();

            dg.ItemsSource = c.Normativi.Local.ToObservableCollection().Where(i => i.sifra_artikla == int.Parse(sifraArtikla));

            if (dg.Items.Count > 0)
            {
                dg.Columns.RemoveAt(0);
                dg.Columns.RemoveAt(0);
                dg.Columns.RemoveAt(1);

                dg.Columns[0].Header = "Šifra normativa";
                dg.Columns[1].Header = "Naziv normativa";
                dg.Columns[2].Header = "Količina";
            }
        }

        public void AddValuesToComboboxes(ComboBox JM, ComboBox Grupa, ComboBox Kategorija)
        {
            // Brisanje postojećih vrijednosti u comboboxevima
            JM.Items.Clear();
            Grupa.Items.Clear();
            Kategorija.Items.Clear();

            // Učitavanje vrijednosti iz baze podataka - tablice - jed_mj, grupa i kategorija
            var jedinice_mjere = c.Jed_mj;
            var grupe = c.Grupa;
            var kategorije = c.Kategorija;

            // Dodavanje vrijednosti u comboboxeve
            foreach (var j in jedinice_mjere)
            {
                JM.Items.Add(j.naziv_JM);
            }
            foreach (var g in grupe)
            {
                Grupa.Items.Add(g.naziv_grupe);
            }
            foreach (var k in kategorije)
            {
                Kategorija.Items.Add(k.naziv_kategorije);
            }
        }

        // Ukoliko korisnik pritisne na neki od artikala, tada ažuriramo podatke
        public void ItemSelected(DataGrid dg_artikli, DataGrid dg_normativi, TextBox sifraArtikla, TextBox nazivArtikla, TextBox osnovica, ComboBox JM, ComboBox grupa, ComboBox kategorija, CheckBox vidljivost, Button Save, Button Cancel, Button Remove)
        {

            // Postavi tekstove
            try
            {
                // Uzmi selektirani redak
                Artikl artikl = (Artikl)dg_artikli.SelectedItem;

                if (artikl != null)
                {
                    sifraArtikla.Text = artikl.sifra_artikla.ToString();
                    nazivArtikla.Text = artikl.ime_artikla;
                    osnovica.Text = artikl.osnovica.ToString();
                    vidljivost.IsChecked = artikl.vidljivost;
                    JM.Text = c.Jed_mj.Where(a => a.sifra_JM == artikl.JedinicaMjereID).Single().naziv_JM;
                    grupa.Text = c.Grupa.Where(a => a.sifra_grupe == artikl.grupaID).Single().naziv_grupe;
                    kategorija.Text = c.Kategorija.Where(a => a.sifra_kategorije == artikl.kategorijaID).Single().naziv_kategorije;
                    Save.IsEnabled = true;
                    Cancel.IsEnabled = true;
                    Remove.IsEnabled = true;
                    sifraArtikla.IsEnabled = true;
                    nazivArtikla.IsEnabled = true;
                    osnovica.IsEnabled = true;
                    JM.IsEnabled = true;
                    grupa.IsEnabled = true;
                    kategorija.IsEnabled = true;
                    vidljivost.IsEnabled = true;
                    LoadNormativi(dg_normativi, sifraArtikla.Text);
                }
            }
            catch
            {
                UnselectProduct(dg_artikli, dg_normativi, sifraArtikla, nazivArtikla, osnovica, JM, grupa, kategorija, vidljivost, Save, Cancel, Remove);
            }
        }

        public void UnselectProduct(DataGrid dg_artikli, DataGrid dg_normativi, TextBox sifraArtikla, TextBox nazivArtikla, TextBox osnovica, ComboBox JM, ComboBox grupa, ComboBox kategorija, CheckBox vidljivost, Button Save, Button Cancel, Button Remove)
        {
            dg_artikli.SelectedItem = null;
            dg_normativi.SelectedItem = null;
            sifraArtikla.Text = "";
            nazivArtikla.Text = "";
            osnovica.Text = "";
            vidljivost.IsChecked = true;
            JM.Text = "";
            grupa.Text = "";
            kategorija.Text = "";
            Save.IsEnabled = false;
            Cancel.IsEnabled = false;
            Remove.IsEnabled = false;
            sifraArtikla.IsEnabled = false;
            nazivArtikla.IsEnabled = false;
            osnovica.IsEnabled = false;
            JM.IsEnabled = false;
            grupa.IsEnabled = false;
            kategorija.IsEnabled = false;
            vidljivost.IsEnabled = false;
        }

        // Brisanje artikla 
        public void RemoveProduct(DataGrid dg, DataGrid dg_n, TextBox sifraArtikla, TextBox nazivArtikla, TextBox osnovica, ComboBox JM, ComboBox grupa, ComboBox kategorija, CheckBox vidljivost, Button Save, Button Cancel, Button Remove)
        {
            if (MessageBox.Show("Jeste li sigurni da želite obrisati odabrani artikl?", "Brisanje artikla", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
            {
                // Uzmi selektirani redak
                Artikl artikl = (Artikl)dg.SelectedItem;
                UnselectProduct(dg, dg_n, sifraArtikla, nazivArtikla, osnovica, JM, grupa, kategorija, vidljivost, Save, Cancel, Remove);
                c.Artikli.Remove(artikl);
                c.SaveChanges();
            }
        }

        // Pritiskom na gumb za novi proizvod počistimo vrijednosti u elementima
        public void NewProduct(DataGrid dg, TextBox sifraArtikla, TextBox nazivArtikla, TextBox osnovica, ComboBox JM, ComboBox grupa, ComboBox kategorija, CheckBox vidljivost, Button Save, Button Cancel)
        {
            AddValuesToComboboxes(JM, grupa, kategorija);
            JM.SelectedIndex = 0;
            grupa.SelectedIndex = 0;
            kategorija.SelectedIndex = 0;
            sifraArtikla.Text = "";
            nazivArtikla.Text = "";
            osnovica.Text = "";
            vidljivost.IsChecked = true;
            JM.Text = "";
            grupa.Text = "";
            kategorija.Text = "";
            dg.SelectedItem = null;
            Save.IsEnabled = true;
            Cancel.IsEnabled = true;
            sifraArtikla.IsEnabled = true;
            nazivArtikla.IsEnabled = true;
            osnovica.IsEnabled = true;
            JM.IsEnabled = true;
            grupa.IsEnabled = true;
            kategorija.IsEnabled = true;
            vidljivost.IsEnabled = true;
            sifraArtikla.Focus();
        }

        // Dodavanje novog artikla i spremanje
        public void AddAndSaveChanges(DataGrid dg, DataGrid dg_n, TextBox sifraArtikla, TextBox nazivArtikla, TextBox osnovica, ComboBox JM, ComboBox grupa, ComboBox kategorija, CheckBox vidljivost, Button Save, Button Cancel, Button Remove)
        {
            // Provjera praznih textboxeva
            if (sifraArtikla.Text == "" || nazivArtikla.Text == "" || osnovica.Text == "" || JM.Text == "" || grupa.Text == "" || kategorija.Text == "")
            {
                MessageBox.Show("Trebate ispuniti sva područja kako biste uspješno dodali ili uredili artikl.");
                return;
            }
            try
            {
                // Uzimanje selektiranog artikla kako bismo provjerili radi li se o uređivanju ili dodavanju novog proizvoda
                int selectedArtiklID = 0;
                Artikl selectedArtikl = (Artikl)dg.SelectedItem;
                if (selectedArtikl != null)
                {
                    selectedArtiklID = selectedArtikl.sifra_artikla;
                }

                // Definiranje novog artikla
                Artikl a = new Artikl();
                if (sifraArtikla.Text.Length > 0)
                    a.sifra_artikla = int.Parse(sifraArtikla.Text);
                a.ime_artikla = nazivArtikla.Text;
                a.osnovica = double.Parse(osnovica.Text);
                a.putanja_do_slike = "";
                if (vidljivost.IsChecked == true)
                    a.vidljivost = true;
                else
                    a.vidljivost = false;
                a.JedinicaMjereID = c.Jed_mj.Where(b => b.naziv_JM == JM.Text).Single().sifra_JM;
                a.kategorijaID = c.Kategorija.Where(b => b.naziv_kategorije == kategorija.Text).Single().sifra_kategorije;
                a.grupaID = c.Grupa.Where(b => b.naziv_grupe == grupa.Text).Single().sifra_grupe;

                // Ukoliko je uređivanje, potrebno je provjeriti po šifri ili nazivu artikla postoji li navedeni artikl te ga brisemo i dodajemo ažurirani
                if (selectedArtikl != null)
                {
                    if (c.Artikli.Where(b => b.sifra_artikla == selectedArtiklID).Single() != null || c.Artikli.Where(b => b.ime_artikla == selectedArtikl.ime_artikla).Single() != null)
                    {
                        c.Artikli.Remove(selectedArtikl);
                        c.Artikli.Add(a);

                        dg.ItemsSource = null;
                        dg.ItemsSource = c.Artikli.Local.ToObservableCollection();
                        UnselectProduct(dg, dg_n, sifraArtikla, nazivArtikla, osnovica, JM, grupa, kategorija, vidljivost, Save, Cancel, Remove);

                        // Uređivanje stupaca
                        dg.Columns.RemoveAt(6);
                        dg.Columns.RemoveAt(7);
                        dg.Columns.RemoveAt(8);
                        dg.Columns.RemoveAt(8);

                        dg.Columns[0].Header = "Šifra artikla";
                        dg.Columns[1].Header = "Naziv artikla";
                        dg.Columns[2].Header = "Osnovica";
                        dg.Columns[3].Header = "Putanja do slike";
                        dg.Columns[4].Header = "Vidljivost";
                    }
                }

                else
                {
                    // Dodavanje u tablicu
                    c.Artikli.Add(a);
                }
                UnselectProduct(dg, dg_n, sifraArtikla, nazivArtikla, osnovica, JM, grupa, kategorija, vidljivost, Save, Cancel, Remove);

                Save.IsEnabled = true;
                Cancel.IsEnabled = true;
                sifraArtikla.IsEnabled = true;
                nazivArtikla.IsEnabled = true;
                osnovica.IsEnabled = true;
                JM.IsEnabled = true;
                grupa.IsEnabled = true;
                kategorija.IsEnabled = true;
                vidljivost.IsEnabled = true;
                sifraArtikla.Focus();
                // Spremanje promjena
                c.SaveChanges();
            }
            catch
            {
                MessageBox.Show("Greška prilikom dodavanja ili uređivanja artikla.");
            }
        }

        // Odustajanje od dodavanja novog artikla
        public void CancelNewEntry(DataGrid dg_artikli, DataGrid dg_normativi, TextBox sifraArtikla, TextBox nazivArtikla, TextBox osnovica, ComboBox JM, ComboBox grupa, ComboBox kategorija, CheckBox vidljivost, Button Save, Button Cancel, Button Remove)
        {
            UnselectProduct(dg_artikli, dg_normativi, sifraArtikla, nazivArtikla, osnovica, JM, grupa, kategorija, vidljivost, Save, Cancel,Remove);
        }
    }
}
