﻿using FiskalVictory.DataAccess;
using FiskalVictory.MVVM.Models;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Channels;
using System.Windows;
using System.Windows.Controls;

namespace FiskalVictory.MVVM.ViewModels
{
    public class NormativiViewModel
    {
        // Pristupanje bazi podataka
        RacunContext c = new RacunContext();
        public void Load(DataGrid dg, ComboBox Artikl)
        {
            // Čitanje svih redaka 
            c.Normativi.Load();

            dg.ItemsSource = c.Normativi.Local.ToObservableCollection();

            dg.Columns.RemoveAt(2);

            dg.Columns[0].Header = "Šifra normativa";
            dg.Columns[1].Header = "Šifra artikla";
            dg.Columns[2].Header = "Naziv normativa";
            dg.Columns[3].Header = "Količina";
            AddValueToCombobox(Artikl);
        }
        public void AddValueToCombobox(ComboBox Artikl)
        {
            // Brisanje postojećih vrijednosti u comboboxu
            Artikl.Items.Clear();

            // Učitavanje vrijednosti iz baze podataka
            var artikli = c.Artikli;

            // Dodavanje vrijednosti u comboboxeve
            foreach (var a in artikli)
            {
                Artikl.Items.Add(a.ime_artikla);
            }
        }

        // Ukoliko korisnik pritisne na neki od redaka, tada ažuriramo podatke
        public void ItemSelected(DataGrid dg, TextBox sifra_normativa, ComboBox naziv_artikla, TextBox naziv_normativa, TextBox kolicina, Button Save, Button Cancel, Button Remove)
        {
            // Postavi tekstove
            try
            {
                // Uzmi selektirani redak
                Normativ n = (Normativ)dg.SelectedItem;

                if (n != null)
                {
                    sifra_normativa.Text = n.sifra_normativa.ToString();
                    naziv_normativa.Text = n.naziv_normativa;
                    kolicina.Text = n.kolicina.ToString();
                    naziv_artikla.Text = c.Artikli.Where(a => a.sifra_artikla == n.sifra_artikla).Single().ime_artikla;
                    Save.IsEnabled = true;
                    Cancel.IsEnabled = true;
                    Remove.IsEnabled = true;
                    sifra_normativa.IsEnabled = true;
                    naziv_normativa.IsEnabled = true;
                    naziv_artikla.IsEnabled = true;
                    kolicina.IsEnabled = true;
                }
            }
            catch
            {
                Unselect(dg, sifra_normativa, naziv_artikla, naziv_normativa, kolicina, Save, Cancel, Remove);
            }
        }

        public void Unselect(DataGrid dg, TextBox sifra_normativa, ComboBox naziv_artikla, TextBox naziv_normativa, TextBox kolicina, Button Save, Button Cancel, Button Remove)
        {
            dg.SelectedItem = null;
            sifra_normativa.Text = "";
            naziv_artikla.SelectedItem = null;
            naziv_normativa.Text = "";
            kolicina.Text = "";
            Save.IsEnabled = false;
            Cancel.IsEnabled = false;
            Remove.IsEnabled = false;
            sifra_normativa.IsEnabled = false;
            naziv_normativa.IsEnabled = false;
            naziv_artikla.IsEnabled = false;
            kolicina.IsEnabled = false;
        }

        // Brisanje retka 
        public void Remove(DataGrid dg, TextBox sifra_normativa, ComboBox naziv_artikla, TextBox naziv_normativa, TextBox kolicina, Button Save, Button Cancel, Button Remove)
        {
            if (MessageBox.Show("Jeste li sigurni da želite obrisati odabrani normativ?", "Brisanje normativa", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
            {
                // Uzmi selektirani redak
                Normativ n = (Normativ)dg.SelectedItem;
                Unselect(dg, sifra_normativa, naziv_artikla, naziv_normativa, kolicina, Save, Cancel, Remove);
                c.Normativi.Remove(n);
                c.SaveChanges();
            }
        }

        // Pritiskom na gumb za novi proizvod počistimo vrijednosti u elementima
        public void New(DataGrid dg, TextBox sifra_normativa, ComboBox naziv_artikla, TextBox naziv_normativa, TextBox kolicina, Button Save, Button Cancel)
        {
            sifra_normativa.Text = "";
            naziv_artikla.Text = "";
            naziv_normativa.Text = "";
            kolicina.Text = "";
            dg.SelectedItem = null;
            Save.IsEnabled = true;
            Cancel.IsEnabled = true;
            sifra_normativa.IsEnabled = true;
            naziv_normativa.IsEnabled = true;
            naziv_artikla.IsEnabled = true;
            kolicina.IsEnabled = true;
            sifra_normativa.Focus();
        }

        // Dodavanje novog retka i spremanje
        public void AddAndSaveChanges(DataGrid dg, TextBox sifra_normativa, ComboBox naziv_artikla, TextBox naziv_normativa, TextBox kolicina, Button Save, Button Cancel, Button Remove)
        {
            // Provjera praznih textboxeva
            if (sifra_normativa.Text == "" || naziv_artikla.Text == "" || naziv_artikla.Text == "" || naziv_normativa.Text == "" ||kolicina.Text == "")
            {
                MessageBox.Show("Trebate ispuniti sva područja kako biste uspješno dodali ili uredili normativ.");
                return;
            }
            try
            {
                // Uzimanje selektiranog retka kako bismo provjerili radi li se o uređivanju ili dodavanju novog retka
                int selectedID = 0;
                Normativ selectedItem = (Normativ)dg.SelectedItem;
                if (selectedItem != null)
                {
                    selectedID = selectedItem.sifra_normativa;
                }

                // Definiranje novog unosa
                Normativ item = new Normativ();
                item.sifra_normativa = int.Parse(sifra_normativa.Text);
                item.naziv_normativa = naziv_normativa.Text;
                item.kolicina = double.Parse(kolicina.Text);
                item.sifra_artikla = c.Artikli.Where(b => b.ime_artikla == naziv_artikla.Text).Single().sifra_artikla;

                // Ukoliko je uređivanje, potrebno je provjeriti 
                if (selectedItem != null)
                {
                    if (c.Normativi.Where(b => b.sifra_normativa == selectedID && b.sifra_artikla == selectedItem.sifra_artikla).Single() != null)
                    {
                        c.Normativi.Remove(selectedItem);
                        c.Normativi.Add(item);

                        dg.ItemsSource = null;
                        dg.ItemsSource = c.Normativi.Local.ToObservableCollection();
                        Unselect(dg, sifra_normativa, naziv_artikla, naziv_normativa, kolicina, Save, Cancel, Remove);

                        dg.Columns.RemoveAt(2);

                        dg.Columns[0].Header = "Šifra normativa";
                        dg.Columns[1].Header = "Šifra artikla";
                        dg.Columns[2].Header = "Naziv normativa";
                        dg.Columns[3].Header = "Količina";
                    }
                }

                else
                {
                    // Dodavanje u tablicu
                    c.Normativi.Add(item);
                }
                Unselect(dg, sifra_normativa, naziv_artikla, naziv_normativa, kolicina, Save, Cancel, Remove);

                Save.IsEnabled = true;
                Cancel.IsEnabled = true;
                sifra_normativa.IsEnabled = true;
                naziv_normativa.IsEnabled = true;
                naziv_artikla.IsEnabled = true;
                kolicina.IsEnabled = true;
                sifra_normativa.Focus();
                // Spremanje promjena
                c.SaveChanges();
            }
            catch
            {
                MessageBox.Show("Greška prilikom dodavanja ili uređivanja normativa.");
            }
        }

        // Odustajanje od dodavanja novog unosa
        public void CancelNewEntry(DataGrid dg, TextBox sifra_normativa, ComboBox naziv_artikla, TextBox naziv_normativa, TextBox kolicina, Button Save, Button Cancel, Button Remove)
        {
            Unselect(dg, sifra_normativa, naziv_artikla, naziv_normativa, kolicina, Save, Cancel, Remove);
        }
    }
}
