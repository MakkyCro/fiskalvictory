﻿using FiskalVictory.DataAccess;
using FiskalVictory.MVVM.Models;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Channels;
using System.Windows;
using System.Windows.Controls;

namespace FiskalVictory.MVVM.ViewModels
{
    public class NacinPlacanjaViewModel
    {
        // Pristupanje bazi podataka
        RacunContext c = new RacunContext();
        public void Load(DataGrid dg)
        {
            // Čitanje svih redaka 
            c.NacinPlacanja.Load();

            dg.ItemsSource = c.NacinPlacanja.Local.ToObservableCollection();

            dg.Columns.RemoveAt(2);

            dg.Columns[0].Header = "Šifra načina plaćanja";
            dg.Columns[1].Header = "Naziv načina plaćanja";
        }

        // Ukoliko korisnik pritisne na neki od redaka, tada ažuriramo podatke
        public void ItemSelected(DataGrid dg, TextBox sifra_nac_pl, TextBox naziv_nac_pl, Button Save, Button Cancel, Button Remove)
        {
            // Postavi tekstove
            try
            {
                // Uzmi selektirani redak
                NacinPlacanja np = (NacinPlacanja)dg.SelectedItem;

                if (np != null)
                {
                    sifra_nac_pl.Text = np.sifra_placanja.ToString();
                    naziv_nac_pl.Text = np.naziv_placanja;
                    Save.IsEnabled = true;
                    Cancel.IsEnabled = true;
                    Remove.IsEnabled = true;
                    sifra_nac_pl.IsEnabled = true;
                    naziv_nac_pl.IsEnabled = true;
                }
            }
            catch
            {
                Unselect(dg, sifra_nac_pl, naziv_nac_pl, Save, Cancel, Remove);
            }
        }

        public void Unselect(DataGrid dg, TextBox sifra_nac_pl, TextBox naziv_nac_pl, Button Save, Button Cancel, Button Remove)
        {
            dg.SelectedItem = null;
            sifra_nac_pl.Text = "";
            naziv_nac_pl.Text = "";
            Save.IsEnabled = false;
            Cancel.IsEnabled = false;
            Remove.IsEnabled = false;
            sifra_nac_pl.IsEnabled = false;
            naziv_nac_pl.IsEnabled = false;
        }

        // Brisanje retka 
        public void Remove(DataGrid dg, TextBox sifra_nac_pl, TextBox naziv_nac_pl, Button Save, Button Cancel, Button Remove)
        {
            if (MessageBox.Show("Jeste li sigurni da želite obrisati odabranu grupu?", "Brisanje grupe", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
            {
                // Uzmi selektirani redak
                NacinPlacanja np = (NacinPlacanja)dg.SelectedItem;
                Unselect(dg, sifra_nac_pl, naziv_nac_pl, Save, Cancel, Remove);
                c.NacinPlacanja.Remove(np);
                c.SaveChanges();
            }
        }

        // Pritiskom na gumb za novi proizvod počistimo vrijednosti u elementima
        public void New(DataGrid dg, TextBox sifra_nac_pl, TextBox naziv_nac_pl, Button Save, Button Cancel)
        {
            sifra_nac_pl.Text = "";
            naziv_nac_pl.Text = "";
            dg.SelectedItem = null;
            Save.IsEnabled = true;
            Cancel.IsEnabled = true;
            sifra_nac_pl.IsEnabled = true;
            naziv_nac_pl.IsEnabled = true;
            sifra_nac_pl.Focus();
        }

        // Dodavanje novog retka i spremanje
        public void AddAndSaveChanges(DataGrid dg, TextBox sifra_nac_pl, TextBox naziv_nac_pl, Button Save, Button Cancel, Button Remove)
        {
            // Provjera praznih textboxeva
            if (sifra_nac_pl.Text == "" || naziv_nac_pl.Text == "")
            {
                MessageBox.Show("Trebate ispuniti sva područja kako biste uspješno dodali ili uredili grupu.");
                return;
            }
            try
            {
                // Uzimanje selektiranog retka kako bismo provjerili radi li se o uređivanju ili dodavanju novog retka
                int selectedID = 0;
                NacinPlacanja selectedItem = (NacinPlacanja)dg.SelectedItem;
                if (selectedItem != null)
                {
                    selectedID = selectedItem.sifra_placanja;
                }

                // Definiranje novog unosa
                NacinPlacanja item = new NacinPlacanja();
                item.sifra_placanja = int.Parse(sifra_nac_pl.Text);
                item.naziv_placanja = naziv_nac_pl.Text;

                // Ukoliko je uređivanje, potrebno je provjeriti 
                if (selectedItem != null)
                {
                    if (c.NacinPlacanja.Where(b => b.sifra_placanja == selectedID).Single() != null)
                    {
                        c.NacinPlacanja.Remove(selectedItem);
                        c.NacinPlacanja.Add(item);

                        dg.ItemsSource = null;
                        dg.ItemsSource = c.NacinPlacanja.Local.ToObservableCollection();
                        Unselect(dg, sifra_nac_pl, naziv_nac_pl, Save, Cancel, Remove);

                        // Uređivanje stupaca

                        dg.Columns.RemoveAt(2);

                        dg.Columns[0].Header = "Šifra načina plaćanja";
                        dg.Columns[1].Header = "Naziv načina plaćanja";
                    }
                }

                else
                {
                    // Dodavanje u tablicu
                    c.NacinPlacanja.Add(item);
                }
                Unselect(dg, sifra_nac_pl, naziv_nac_pl, Save, Cancel, Remove);
                Save.IsEnabled = true;
                Cancel.IsEnabled = true;
                sifra_nac_pl.IsEnabled = true;
                naziv_nac_pl.IsEnabled = true;
                sifra_nac_pl.Focus();
                // Spremanje promjena
                c.SaveChanges();
            }
            catch
            {
                MessageBox.Show("Greška prilikom dodavanja ili uređivanja načina plaćanja.");
            }
        }

        // Odustajanje od dodavanja novog unosa
        public void CancelNewEntry(DataGrid dg, TextBox sifra_nac_pl, TextBox naziv_nac_pl, Button Save, Button Cancel, Button Remove)
        {
            Unselect(dg, sifra_nac_pl, naziv_nac_pl, Save, Cancel, Remove);
        }
    }
}
