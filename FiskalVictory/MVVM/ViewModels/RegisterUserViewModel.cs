﻿using FiskalVictory.Core;
using FiskalVictory.DataAccess;
using FiskalVictory.MVVM.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Windows;
using System.Windows.Controls;

namespace FiskalVictory.MVVM.ViewModels
{
    public class RegisterUserViewModel
    {
        Licence licence = Licence.Lic;
        Location location = Location.LocationManager;
        SelectFromDB getFromDB = SelectFromDB.GetFromDataBase;

        public void LoadDataIntoDBVM(ComboBox cb_drzava_vlasnika, ComboBox cb_drzava_pp, ComboBox cb_zupanija_vlasnika, ComboBox cb_zupanija_pp)
        {
            location.LoadIntoCB(cb_drzava_vlasnika, location.drzave, 1);
            location.LoadIntoCB(cb_drzava_pp, location.drzave, 1);
            cb_drzava_vlasnika.SelectedIndex = 0;
            cb_drzava_pp.SelectedIndex = 0;
            location.LoadIntoCB(cb_zupanija_vlasnika, location.zupanije, 2);
            location.LoadIntoCB(cb_zupanija_pp, location.zupanije, 2);
        }

        public void SubmitPressed(Button btn_submitInformations, TextBox OIB_vlasnika, TextBox ime_vlasnika, TextBox prezime_vlasnika, TextBox adresa_vlasnika, TextBox email_vlasnika, TextBox telefon_vlasnika, TextBox naziv_posl_pr, TextBox adresa_posl_pr, TextBox fina_certifikat, TextBox radno_vrijeme, TextBox email_pp, TextBox telefon_pp, TextBox naziv_kase, PasswordBox sifra_administratora, ComboBox cb_drzava_vlasnika, ComboBox cb_zupanija_vlasnika, ComboBox cb_opcina_vlasnika, ComboBox cb_mjesto_vlasnika, ComboBox cb_drzava_pp, ComboBox cb_zupanija_pp, ComboBox cb_opcina_pp, ComboBox cb_mjesto_pp)
        {

            if (OIB_vlasnika.Text.Length > 10 && ime_vlasnika.Text.Length > 0 && prezime_vlasnika.Text.Length > 0 && adresa_vlasnika.Text.Length > 0 && email_vlasnika.Text.Length > 0 && email_vlasnika.Text.Contains("@") && telefon_vlasnika.Text.Length > 0 && naziv_posl_pr.Text.Length > 0 && adresa_posl_pr.Text.Length > 0 && fina_certifikat.Text.Length > 0 && email_pp.Text.Length > 0 && email_pp.Text.Contains("@") && telefon_pp.Text.Length > 0 && naziv_kase.Text.Length > 0 && cb_mjesto_vlasnika.Text.Length > 0 && cb_mjesto_pp.Text.Length > 0)
            {
                FiskalVictoryWindow mainwindow = FiskalVictoryWindow.MainWindowFV;

                btn_submitInformations.IsEnabled = false;
                // Dobivanje indexa za mjesto, općinu, županiju i državu (za unos)
                string[] userLocationIDs = location.getLocationIDs(cb_drzava_vlasnika.Text, cb_zupanija_vlasnika.Text, cb_opcina_vlasnika.Text, cb_mjesto_vlasnika.Text);
                string[] businessLocationIDs = location.getLocationIDs(cb_drzava_pp.Text, cb_zupanija_pp.Text, cb_opcina_pp.Text, cb_mjesto_pp.Text);

                // Polje koje sadrži podatke o vlasniku poslovnog prostora
                string[] firma = {
                    OIB_vlasnika.Text, ime_vlasnika.Text, prezime_vlasnika.Text, email_vlasnika.Text, telefon_vlasnika.Text, adresa_vlasnika.Text
                };

                // Polje koje sadrži podatke o poslovnom prostoru
                string[] posl_pr =
                {
                    OIB_vlasnika.Text, naziv_posl_pr.Text, adresa_posl_pr.Text, fina_certifikat.Text,radno_vrijeme.Text, email_pp.Text, telefon_pp.Text
                };

                // Polje koje sadrži podatke o kasi
                string[] kasa = { OIB_vlasnika.Text, userdata.Default.id_uredaja, naziv_kase.Text, sifra_administratora.Password.ToString() };

                // Slanje podataka u bazu podataka
                licence.sendLicenceRequest(firma, posl_pr, kasa, userLocationIDs, businessLocationIDs);
                userdata.Default.oib_vlasnika = OIB_vlasnika.Text;
                userdata.Default.sifra_admina = sifra_administratora.Password.ToString();
                userdata.Default.sifra_pp = getFromDB.GetValueFromDB("select sifra_pp from kase where id_uredaja like '" + userdata.Default.id_uredaja + "'");
                userdata.Default.sifra_kase = getFromDB.GetValueFromDB("select sifra_kase from kase where id_uredaja like '" + userdata.Default.id_uredaja + "'");
                userdata.Default.ime_vlasnika = ime_vlasnika.Text;
                userdata.Default.prezime_vlasnika = prezime_vlasnika.Text;
                userdata.Default.naziv_pp = naziv_posl_pr.Text;
                userdata.Default.adresa_pp = adresa_posl_pr.Text;
                userdata.Default.mjesto_pp = cb_mjesto_pp.Text;
                userdata.Default.opcina_pp = cb_opcina_pp.Text;
                userdata.Default.drzava_pp = cb_drzava_pp.Text;
                userdata.Default.adresa_user = adresa_vlasnika.Text;
                userdata.Default.mjesto_user = cb_mjesto_vlasnika.Text;
                userdata.Default.opcina_user = cb_opcina_vlasnika.Text;
                userdata.Default.drzava_user = cb_drzava_vlasnika.Text;
                userdata.Default.Save();
                userdata.Default.Reload();

                RacunContext c = new RacunContext();
                Radnik r = new Radnik();
                r.oib_radnika = userdata.Default.oib_vlasnika;
                r.ime_radnika = userdata.Default.ime_vlasnika;
                r.prezime_radnika = userdata.Default.prezime_vlasnika;
                r.lozinka_radnika = userdata.Default.sifra_admina;
                r.smjena = "/";
                r.broj_telefona = telefon_vlasnika.Text;
                c.Radnici.Add(r);
                c.SaveChanges();
                c.Dispose();

                string sadrzaj = string.Format("Novi korisnik je zatražio licencu.\n\n" +
                    "Datum i vrijeme:\n{0}\n\n" +
                    "Detalji o korisniku\n" +
                    "OIB vlasnika: {1}\n" +
                    "Ime vlasnika: {2}\n" +
                    "Prezime vlasnika: {3}\n" +
                    "email vlasnika: {4}\n" +
                    "Telefon vlasnika: {5}\n" +
                    "Adresa vlasnika: {6}\n\n" +
                    "Detalji o poslovnom prostoru\n" +
                    "Naziv poslovnog prostora: {7}\n" +
                    "Adresa poslovnog prostora: {8}\n" +
                    "FINA certifikat: {9}\n" +
                    "Radno vrijeme: {10}\n" +
                    "Email poslovnog prostora: {11}\n" +
                    "Telefon poslovnog prostora: {12}\n\n" +
                    "Detalji o uređaju:\n" +
                    "ID uređaja: {13}\n" +
                    "Naziv kase: {14}\n" +
                    "Šifra administratora: {15}\n\n",
                    DateTime.Now.ToString("ddd", new System.Globalization.CultureInfo("hr-HR")).ToUpper() + DateTime.Now.ToString(", dd. MMMM yyyy.", new System.Globalization.CultureInfo("hr-HR")) + " - " + DateTime.Now.ToString("HH:mm:ss"),
                    firma[0],
                    firma[1],
                    firma[2],
                    firma[3],
                    firma[4],
                    (firma[5] + ", "
                    + location.getLocationNameByID(location.mjesta, 3, int.Parse(userLocationIDs[3]))
                    ),
                    posl_pr[1],
                    (posl_pr[2] + ", "
                    + location.getLocationNameByID(location.mjesta, 3, int.Parse(businessLocationIDs[3]))
                    ),
                    posl_pr[3],
                    posl_pr[4],
                    posl_pr[5],
                    posl_pr[6],
                    kasa[1],
                    kasa[2],
                    kasa[3]
                    );

                EmailController email = new EmailController();
                var response = email.sendEmail("jasmin.makaj@gmail.com", "Jasmin", "Registracija novog korisnika", sadrzaj);

                mainwindow.RegisterPressed();
            }
            else
            {
                MessageBox.Show("Niste unijeli sve podatke ili su neki od unešenih podataka krivi.");
            }
        }

    }
}
