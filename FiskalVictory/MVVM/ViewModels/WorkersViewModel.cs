﻿using FiskalVictory.DataAccess;
using FiskalVictory.MVVM.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;

namespace FiskalVictory.MVVM.ViewModels
{
    public class WorkersViewModel
    {
        // Pristupanje bazi podataka
        RacunContext c = new RacunContext();
        public void LoadWorkers(DataGrid dg_workers)
        {
            // Čitanje svih redaka artikala
            c.Radnici.Load();

            dg_workers.ItemsSource = c.Radnici.Local.ToObservableCollection();

            dg_workers.Columns.RemoveAt(6);

            dg_workers.Columns[0].Header = "OIB radnika";
            dg_workers.Columns[1].Header = "Ime radnika";
            dg_workers.Columns[2].Header = "Prezime radnika";
            dg_workers.Columns[3].Header = "Lozinka za prijavu";
            dg_workers.Columns[4].Header = "Smjena";
            dg_workers.Columns[5].Header = "Broj telefona";
        }

        // Ukoliko korisnik pritisne na neki od artikala, tada ažuriramo podatke
        public void ItemSelected(DataGrid dg_workers, TextBox oib_radnika, TextBox ime_radnika, TextBox prezime_radnika, TextBox lozinka_radnika, TextBox smjena, TextBox broj_telefona, Button Save, Button Cancel, Button Remove)
        {
            // Postavi tekstove
            try
            {
                // Uzmi selektirani redak
                Radnik radnik = (Radnik)dg_workers.SelectedItem;

                if (radnik != null)
                {
                    oib_radnika.Text = radnik.oib_radnika;
                    ime_radnika.Text = radnik.ime_radnika;
                    prezime_radnika.Text = radnik.prezime_radnika;
                    lozinka_radnika.Text = radnik.lozinka_radnika;
                    smjena.Text = radnik.smjena;
                    broj_telefona.Text = radnik.broj_telefona;
                    Save.IsEnabled = true;
                    Cancel.IsEnabled = true;
                    Remove.IsEnabled = true;
                    oib_radnika.IsEnabled = true;
                    ime_radnika.IsEnabled = true;
                    prezime_radnika.IsEnabled = true;
                    lozinka_radnika.IsEnabled = true;
                    smjena.IsEnabled = true;
                    broj_telefona.IsEnabled = true;
                }
            }
            catch
            {
                UnselectWorker(dg_workers, oib_radnika, ime_radnika, prezime_radnika, lozinka_radnika, smjena, broj_telefona, Save, Cancel, Remove);
            }
        }

        public void UnselectWorker(DataGrid dg, TextBox oib_radnika, TextBox ime_radnika, TextBox prezime_radnika, TextBox lozinka_radnika, TextBox smjena, TextBox broj_telefona, Button Save, Button Cancel, Button Remove)
        {
            dg.SelectedItem = null;
            oib_radnika.Text = "";
            ime_radnika.Text = "";
            prezime_radnika.Text = "";
            lozinka_radnika.Text = "";
            smjena.Text = "";
            broj_telefona.Text = "";
            Save.IsEnabled = false;
            Cancel.IsEnabled = false;
            Remove.IsEnabled = false;
            oib_radnika.IsEnabled = false;
            ime_radnika.IsEnabled = false;
            prezime_radnika.IsEnabled = false;
            lozinka_radnika.IsEnabled = false;
            smjena.IsEnabled = false;
            broj_telefona.IsEnabled = false;
        }

        // Brisanje radnika 
        public void RemoveWorker(DataGrid dg, TextBox oib_radnika, TextBox ime_radnika, TextBox prezime_radnika, TextBox lozinka_radnika, TextBox smjena, TextBox broj_telefona, Button Save, Button Cancel, Button Remove)
        {
            if (MessageBox.Show("Jeste li sigurni da želite obrisati odabranog radnika?", "Brisanje radnika", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
            {
                // Uzmi selektirani redak
                Radnik radnik = (Radnik)dg.SelectedItem;
                UnselectWorker(dg, oib_radnika, ime_radnika, prezime_radnika, lozinka_radnika, smjena, broj_telefona, Save, Cancel, Remove);
                c.Radnici.Remove(radnik);
                c.SaveChanges();
            }
        }

        // Pritiskom na gumb za novi proizvod počistimo vrijednosti u elementima
        public void NewWorker(DataGrid dg, TextBox oib_radnika, TextBox ime_radnika, TextBox prezime_radnika, TextBox lozinka_radnika, TextBox smjena, TextBox broj_telefona, Button Save, Button Cancel)
        {
            oib_radnika.Text = "";
            ime_radnika.Text = "";
            prezime_radnika.Text = "";
            lozinka_radnika.Text = "";
            smjena.Text = "";
            broj_telefona.Text = "";
            dg.SelectedItem = null;
            Save.IsEnabled = true;
            Cancel.IsEnabled = true;
            oib_radnika.IsEnabled = true;
            ime_radnika.IsEnabled = true;
            prezime_radnika.IsEnabled = true;
            lozinka_radnika.IsEnabled = true;
            smjena.IsEnabled = true;
            broj_telefona.IsEnabled = true;
            oib_radnika.Focus();
        }

        // Dodavanje novog radnika i spremanje
        public void AddAndSaveChanges(DataGrid dg, TextBox oib_radnika, TextBox ime_radnika, TextBox prezime_radnika, TextBox lozinka_radnika, TextBox smjena, TextBox broj_telefona, Button Save, Button Cancel, Button Remove)
        {
            // Provjera praznih textboxeva
            if (oib_radnika.Text == "" || ime_radnika.Text == "" || prezime_radnika.Text == "" || lozinka_radnika.Text == "" || broj_telefona.Text == "")
            {
                MessageBox.Show("Trebate ispuniti sva područja (osim smjena koja je opcionalna) kako biste uspješno dodali ili uredili radnika.");
                return;
            }
            try
            {
                // Uzimanje selektiranog radnika kako bismo provjerili radi li se o uređivanju ili dodavanju novog proizvoda
                string selectedRadnikOIB = "";
                Radnik selectedRadnik = (Radnik)dg.SelectedItem;
                if (selectedRadnik != null)
                {
                    selectedRadnikOIB = selectedRadnik.oib_radnika;
                }

                // Definiranje novog radnika
                Radnik r = new Radnik();
                r.oib_radnika = oib_radnika.Text;
                r.ime_radnika = ime_radnika.Text;
                r.prezime_radnika = prezime_radnika.Text;
                r.lozinka_radnika = lozinka_radnika.Text;
                r.smjena = smjena.Text;
                r.broj_telefona = broj_telefona.Text;

                // Ukoliko je uređivanje, potrebno je provjeriti po OIBU
                if (selectedRadnik != null)
                {
                    if (c.Radnici.Where(b => b.oib_radnika == selectedRadnikOIB).Single() != null)
                    {
                        c.Radnici.Remove(selectedRadnik);
                        c.Radnici.Add(r);

                        dg.ItemsSource = null;
                        dg.ItemsSource = c.Radnici.Local.ToObservableCollection();
                        UnselectWorker(dg, oib_radnika, ime_radnika, prezime_radnika, lozinka_radnika, smjena, broj_telefona, Save, Cancel, Remove);

                        // Uređivanje stupaca

                        dg.Columns.RemoveAt(6);

                        dg.Columns[0].Header = "OIB radnika";
                        dg.Columns[1].Header = "Ime radnika";
                        dg.Columns[2].Header = "Prezime radnika";
                        dg.Columns[3].Header = "Lozinka za prijavu";
                        dg.Columns[4].Header = "Smjena";
                        dg.Columns[5].Header = "Broj telefona";
                    }
                }

                else
                {
                    // Dodavanje u tablicu
                    c.Radnici.Add(r);
                }
                UnselectWorker(dg, oib_radnika, ime_radnika, prezime_radnika, lozinka_radnika, smjena, broj_telefona, Save, Cancel, Remove);

                Save.IsEnabled = true;
                Cancel.IsEnabled = true;
                oib_radnika.IsEnabled = true;
                ime_radnika.IsEnabled = true;
                prezime_radnika.IsEnabled = true;
                lozinka_radnika.IsEnabled = true;
                smjena.IsEnabled = true;
                broj_telefona.IsEnabled = true;
                oib_radnika.Focus();
                // Spremanje promjena
                c.SaveChanges();
            }
            catch
            {
                MessageBox.Show("Greška prilikom dodavanja ili uređivanja artikla.");
            }
        }

        // Odustajanje od dodavanja novog radnika
        public void CancelNewEntry(DataGrid dg, TextBox oib_radnika, TextBox ime_radnika, TextBox prezime_radnika, TextBox lozinka_radnika, TextBox smjena, TextBox broj_telefona, Button Save, Button Cancel, Button Remove)
        {
            UnselectWorker(dg, oib_radnika, ime_radnika, prezime_radnika, lozinka_radnika, smjena, broj_telefona, Save,  Cancel, Remove);
        }
    }
}
