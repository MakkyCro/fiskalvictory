﻿using FiskalVictory.MVVM.ViewModels;
using System.Windows;
using System.Windows.Controls;

namespace FiskalVictory.MVVM.Views
{
    /// <summary>
    /// Interaction logic for Grupa.xaml
    /// </summary>
    public partial class RacuniView : UserControl
    {
        RacuniViewModel rvm = new RacuniViewModel();
        public RacuniView()
        {
            InitializeComponent();
        }

        private void dg_Loaded(object sender, RoutedEventArgs e)
        {
            rvm.Load(dg);
        }

        private void fiskaliziraj(object sender, RoutedEventArgs e)
        {
            rvm.FiskalizirajSve(dg);
        }
    }
}
