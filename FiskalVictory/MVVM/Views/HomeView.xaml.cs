﻿using FiskalVictory.DataAccess;
using FiskalVictory.MVVM.ViewModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;
using System.Xml.Linq;

namespace FiskalVictory.MVVM.Views
{
    /// <summary>
    /// Interaction logic for HomeView.xaml
    /// </summary>
    public partial class HomeView : UserControl
    {

        public static readonly HomeView homeV = new HomeView();

        public static HomeView HomeV
        {
            get
            {
                return homeV;
            }
        }
        public HomeView()
        {
            InitializeComponent();

            mainhomeview.Height = System.Windows.SystemParameters.PrimaryScreenHeight;
            mainhomeview.Width = System.Windows.SystemParameters.PrimaryScreenWidth;
        }

        public void ProductsPressed()
        {
            var nestedviewModel = (NestedViewModel)DataContext;
            nestedviewModel.ProductsVMCommand.Execute(null);
        }

        private void btn_Logout_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            FiskalVictoryWindow mvm = FiskalVictoryWindow.MainWindowFV;
            mvm.LogoutPressed();
        }

        private void btn_izlaz_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            System.Environment.Exit(0);
        }

        private static Action EmptyDelegate = delegate () { };

        private void homeView_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            FiskalVictoryWindow main = FiskalVictoryWindow.MainWindowFV;
            btn_odjava.Content = "ODJAVA (" + main.user + ")";
            RacunContext c = new RacunContext();
            var radnik = c.Radnici.Where(b => b.ime_radnika + " " + b.prezime_radnika == main.user).Single();
            if (userdata.Default.oib_vlasnika == radnik.oib_radnika)
            {
                btn_artikli.Visibility = System.Windows.Visibility.Visible;
                btn_grupe.Visibility = System.Windows.Visibility.Visible;
                btn_JM.Visibility = System.Windows.Visibility.Visible;
                btn_kategorije.Visibility = System.Windows.Visibility.Visible;
                btn_nac_pl.Visibility = System.Windows.Visibility.Visible;
                btn_normativi.Visibility = System.Windows.Visibility.Visible;
                //btn_postavke.Visibility = System.Windows.Visibility.Visible;
                btn_radnici.Visibility = System.Windows.Visibility.Visible;
            }
            c.Dispose();
        }

        private void btnHowTo_Click(object sender, RoutedEventArgs e)
        {
            System.Diagnostics.Process.Start(new ProcessStartInfo
            {
                FileName = "http://test.victory.hr/FiskalVictory/",
                UseShellExecute = true
            });
        }
    }
}
