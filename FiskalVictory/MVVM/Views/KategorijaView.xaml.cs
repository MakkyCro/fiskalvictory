﻿using FiskalVictory.MVVM.ViewModels;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace FiskalVictory.MVVM.Views
{
    /// <summary>
    /// Interaction logic for Grupa.xaml
    /// </summary>
    public partial class KategorijaView : UserControl
    {
        KategorijaViewModel kvm = new KategorijaViewModel();
        public KategorijaView()
        {
            InitializeComponent();
        }

        private void dg_workers_Loaded(object sender, RoutedEventArgs e)
        {
            kvm.Load(dg);
        }

        private void btn_AddWorker_Click(object sender, RoutedEventArgs e)
        {
            kvm.New(dg, sifra_kategorije, naziv_kategorije, tb_PDV, tb_PNP, opis, btn_save, btn_cancel);
        }

        private void btn_removeWorker_Click(object sender, RoutedEventArgs e)
        {
            kvm.Remove(dg, sifra_kategorije, naziv_kategorije, tb_PDV, tb_PNP, opis, btn_save, btn_cancel, btn_remove);
        }

        private void btn_CancelChangesWorker_Click(object sender, RoutedEventArgs e)
        {
            kvm.CancelNewEntry(dg, sifra_kategorije, naziv_kategorije, tb_PDV, tb_PNP, opis, btn_save, btn_cancel, btn_remove);
        }

        private void btn_saveNewWorker_Click(object sender, RoutedEventArgs e)
        {
            kvm.AddAndSaveChanges(dg, sifra_kategorije, naziv_kategorije, tb_PDV, tb_PNP, opis, btn_save, btn_cancel, btn_remove);
        }

        private void dg_workers_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            kvm.ItemSelected(dg, sifra_kategorije, naziv_kategorije, tb_PDV, tb_PNP, opis, btn_save, btn_cancel, btn_remove);
        }

        private void tb_PNP_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
                kvm.AddAndSaveChanges(dg, sifra_kategorije, naziv_kategorije, tb_PDV, tb_PNP, opis, btn_save, btn_cancel, btn_remove);
        }

        private void opis_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
                kvm.AddAndSaveChanges(dg, sifra_kategorije, naziv_kategorije, tb_PDV, tb_PNP, opis, btn_save, btn_cancel, btn_remove);
        }
    }
}
