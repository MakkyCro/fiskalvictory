﻿using FiskalVictory.MVVM.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FiskalVictory.MVVM.Views
{
    /// <summary>
    /// Interaction logic for ProductsView.xaml
    /// </summary>
    public partial class ProductsView : UserControl
    {
        public ProductsView()
        {
            InitializeComponent();
        }

        ProductsViewModel pvm = new ProductsViewModel();
        private void productsView_Loaded(object sender, RoutedEventArgs e)
        {
            pvm.LoadProducts(productsDataGrid, cb_JM, cb_grupa, cb_kategorija);
        }

        private void productsDataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            pvm.ItemSelected(productsDataGrid,normativiDataGrid, tb_sifraArtikla, tb_nazivArtikla, tb_osnovica, cb_JM, cb_grupa, cb_kategorija, cb_vidljivost, btn_save, btn_cancel, btn_remove);
        }

        private void btn_IzbrisiArtikl_Click(object sender, RoutedEventArgs e)
        {
            pvm.RemoveProduct(productsDataGrid, normativiDataGrid, tb_sifraArtikla, tb_nazivArtikla, tb_osnovica, cb_JM, cb_grupa, cb_kategorija, cb_vidljivost, btn_save, btn_cancel, btn_remove);
        }

        private void btn_newArtikl_Click(object sender, RoutedEventArgs e)
        {
            pvm.NewProduct(productsDataGrid, tb_sifraArtikla, tb_nazivArtikla, tb_osnovica, cb_JM, cb_grupa, cb_kategorija, cb_vidljivost, btn_save, btn_cancel);
        }

        private void btn_SaveChanges_Click(object sender, RoutedEventArgs e)
        {
            pvm.AddAndSaveChanges(productsDataGrid, normativiDataGrid, tb_sifraArtikla, tb_nazivArtikla, tb_osnovica, cb_JM, cb_grupa, cb_kategorija, cb_vidljivost, btn_save, btn_cancel, btn_remove);
        }

        private void btn_cancel_Click(object sender, RoutedEventArgs e)
        {
            pvm.CancelNewEntry(productsDataGrid, normativiDataGrid, tb_sifraArtikla, tb_nazivArtikla, tb_osnovica, cb_JM, cb_grupa, cb_kategorija, cb_vidljivost, btn_save, btn_cancel, btn_remove);
        }

        private void cb_grupa_KeyUp(object sender, KeyEventArgs e)
        {
            if(e.Key == Key.Enter)
                pvm.AddAndSaveChanges(productsDataGrid, normativiDataGrid, tb_sifraArtikla, tb_nazivArtikla, tb_osnovica, cb_JM, cb_grupa, cb_kategorija, cb_vidljivost, btn_save, btn_cancel, btn_remove);
        }
    }
}
