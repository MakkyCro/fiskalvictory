﻿using FiskalVictory.MVVM.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FiskalVictory.MVVM.Views
{
    /// <summary>
    /// Interaction logic for Radnici.xaml
    /// </summary>
    public partial class WorkersView : UserControl
    {
        WorkersViewModel wvm = new WorkersViewModel();
        public WorkersView()
        {
            InitializeComponent();
        }

        private void dg_Loaded(object sender, RoutedEventArgs e)
        {
            wvm.LoadWorkers(dg_workers);
        }

        private void btn_Add_Click(object sender, RoutedEventArgs e)
        {
            wvm.NewWorker(dg_workers,oib_radnika,ime_radnika,prezime_radnika,lozinka_radnika,smjena_radnika,broj_telefona, btn_save, btn_cancel);
        }

        private void btn_remove_Click(object sender, RoutedEventArgs e)
        {
            wvm.RemoveWorker(dg_workers, oib_radnika, ime_radnika, prezime_radnika, lozinka_radnika, smjena_radnika, broj_telefona, btn_save, btn_cancel, btn_remove);
        }

        private void btn_CancelChanges_Click(object sender, RoutedEventArgs e)
        {
            wvm.CancelNewEntry(dg_workers, oib_radnika, ime_radnika, prezime_radnika, lozinka_radnika, smjena_radnika, broj_telefona, btn_save, btn_cancel, btn_remove);
        }

        private void btn_saveNew_Click(object sender, RoutedEventArgs e)
        {
            wvm.AddAndSaveChanges(dg_workers, oib_radnika, ime_radnika, prezime_radnika, lozinka_radnika, smjena_radnika, broj_telefona, btn_save, btn_cancel, btn_remove);
        }

        private void dg_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            wvm.ItemSelected(dg_workers, oib_radnika, ime_radnika, prezime_radnika, lozinka_radnika, smjena_radnika, broj_telefona, btn_save, btn_cancel, btn_remove);
        }

        private void broj_telefona_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
                wvm.AddAndSaveChanges(dg_workers, oib_radnika, ime_radnika, prezime_radnika, lozinka_radnika, smjena_radnika, broj_telefona, btn_save, btn_cancel, btn_remove);
        }
    }
}
