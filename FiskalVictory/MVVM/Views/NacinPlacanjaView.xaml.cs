﻿using FiskalVictory.MVVM.ViewModels;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace FiskalVictory.MVVM.Views
{
    /// <summary>
    /// Interaction logic for NacinPlacanja.xaml
    /// </summary>
    public partial class NacinPlacanjaView : UserControl
    {
        NacinPlacanjaViewModel npvm = new NacinPlacanjaViewModel();
        public NacinPlacanjaView()
        {
            InitializeComponent();
        }

        private void dg_Loaded(object sender, RoutedEventArgs e)
        {
            npvm.Load(dg);
        }

        private void btn_Add_Click(object sender, RoutedEventArgs e)
        {
            npvm.New(dg, sifra_nac_pl,naziv_nac_pl, btn_save, btn_cancel);
        }

        private void btn_remove_Click(object sender, RoutedEventArgs e)
        {
            npvm.Remove(dg, sifra_nac_pl, naziv_nac_pl, btn_save, btn_cancel, btn_remove);
        }

        private void btn_CancelChanges_Click(object sender, RoutedEventArgs e)
        {
            npvm.CancelNewEntry(dg, sifra_nac_pl, naziv_nac_pl, btn_save, btn_cancel, btn_remove);
        }

        private void btn_saveNew_Click(object sender, RoutedEventArgs e)
        {
            npvm.AddAndSaveChanges(dg, sifra_nac_pl, naziv_nac_pl, btn_save, btn_cancel, btn_remove);
        }

        private void dg_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            npvm.ItemSelected(dg, sifra_nac_pl, naziv_nac_pl, btn_save, btn_cancel, btn_remove);
        }

        private void naziv_nac_pl_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
                npvm.AddAndSaveChanges(dg, sifra_nac_pl, naziv_nac_pl, btn_save, btn_cancel, btn_remove);
        }
    }
}
