﻿using FiskalVictory.MVVM.ViewModels;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace FiskalVictory.MVVM.Views
{
    /// <summary>
    /// Interaction logic for Grupa.xaml
    /// </summary>
    public partial class GrupaView : UserControl
    {
        GrupaViewModel gvm = new GrupaViewModel();
        public GrupaView()
        {
            InitializeComponent();
        }

        private void dg_Loaded(object sender, RoutedEventArgs e)
        {
            gvm.Load(dg);
        }

        private void btn_Add_Click(object sender, RoutedEventArgs e)
        {
            gvm.New(dg, sifra_grupe, naziv_grupe, btn_save, btn_cancel);
        }

        private void btn_remove_Click(object sender, RoutedEventArgs e)
        {
            gvm.Remove(dg, sifra_grupe, naziv_grupe, btn_save, btn_cancel, btn_remove);
        }

        private void btn_CancelChanges_Click(object sender, RoutedEventArgs e)
        {
            gvm.CancelNewEntry(dg, sifra_grupe, naziv_grupe, btn_save, btn_cancel, btn_remove);
        }

        private void btn_saveNew_Click(object sender, RoutedEventArgs e)
        {
            gvm.AddAndSaveChanges(dg, sifra_grupe, naziv_grupe, btn_save, btn_cancel, btn_remove);
        }

        private void dg_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            gvm.ItemSelected(dg, sifra_grupe, naziv_grupe, btn_save, btn_cancel, btn_remove);
        }

        private void naziv_grupe_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                gvm.AddAndSaveChanges(dg, sifra_grupe, naziv_grupe, btn_save, btn_cancel, btn_remove);
            }
        }
    }
}
