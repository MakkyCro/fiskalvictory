﻿using FiskalVictory.Core;
using FiskalVictory.MVVM.Models;
using FiskalVictory.MVVM.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Windows;
using System.Windows.Controls;

namespace FiskalVictory.MVVM.Views
{
    /// <summary>
    /// Interaction logic for RegisterUserView.xaml
    /// </summary>
    public partial class RegisterUserView : UserControl
    {
        Location location = Location.LocationManager;
        RegisterUserViewModel registervm = new RegisterUserViewModel();

        public RegisterUserView()
        {
            InitializeComponent();
            // Dobivanje svih mjesta, opcina, zupanija i drzava
            location.LoadData();

            // Učitavanje svih država i županija u comboBoxeve
            LoadDataIntoCB();

            Loaded += focusTBoib;
        }

        private void focusTBoib(object sender, RoutedEventArgs e)
        {
            OIB_vlasnika.Focus();
        }

        public void LoadDataIntoCB()
        {
            // Učitavanje država i županija i postavljanje prve države kao defaultna vrijednost koja je označena
            registervm.LoadDataIntoDBVM(cb_drzava_vlasnika, cb_drzava_pp, cb_zupanija_vlasnika, cb_zupanija_pp);
        }

        // Zatraži licencu
        private void btn_submitInformations_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            registervm.SubmitPressed(btn_submitInformations, OIB_vlasnika, ime_vlasnika, prezime_vlasnika, adresa_vlasnika, email_vlasnika, telefon_vlasnika, naziv_posl_pr, adresa_posl_pr, fina_certifikat, radno_vrijeme, email_pp, telefon_pp, naziv_kase, sifra_administratora, cb_drzava_vlasnika, cb_zupanija_vlasnika, cb_opcina_vlasnika, cb_mjesto_vlasnika, cb_drzava_pp, cb_zupanija_pp, cb_opcina_pp, cb_mjesto_pp);
        }

        // Upravljanje comboboxevima
        private void cb_zupanija_vlasnika_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cb_zupanija_vlasnika.SelectedItem != null)
                location.ZupanijaChanged(cb_zupanija_vlasnika.SelectedItem.ToString(), cb_opcina_vlasnika, cb_mjesto_vlasnika);
        }

        private void cb_opcina_vlasnika_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cb_opcina_vlasnika.SelectedItem != null)
                location.OpcinaChanged(cb_opcina_vlasnika.SelectedItem.ToString(), cb_mjesto_vlasnika);
        }

        private void cb_zupanija_pp_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cb_zupanija_pp.SelectedItem != null)
                location.ZupanijaChanged(cb_zupanija_pp.SelectedItem.ToString(), cb_opcina_pp, cb_mjesto_pp);
        }

        private void cb_opcina_pp_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cb_opcina_pp.SelectedItem != null)
                location.OpcinaChanged(cb_opcina_pp.SelectedItem.ToString(), cb_mjesto_pp);
        }
    }
}
