﻿using FiskalVictory.MVVM.ViewModels;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace FiskalVictory.MVVM.Views
{
    /// <summary>
    /// Interaction logic for Grupa.xaml
    /// </summary>
    public partial class NormativiView : UserControl
    {
        NormativiViewModel nvm = new NormativiViewModel();
        public NormativiView()
        {
            InitializeComponent();
        }

        private void dg_Loaded(object sender, RoutedEventArgs e)
        {
            nvm.Load(dg, naziv_artikla);
        }

        private void btn_Add_Click(object sender, RoutedEventArgs e)
        {
            nvm.New(dg, sifra_normativa, naziv_artikla, naziv_normativa, kolicina, btn_save, btn_cancel);
        }

        private void btn_remove_Click(object sender, RoutedEventArgs e)
        {
            nvm.Remove(dg, sifra_normativa, naziv_artikla, naziv_normativa, kolicina, btn_save, btn_cancel, btn_remove);
        }

        private void btn_CancelChanges_Click(object sender, RoutedEventArgs e)
        {
            nvm.CancelNewEntry(dg, sifra_normativa, naziv_artikla, naziv_normativa, kolicina, btn_save, btn_cancel, btn_remove);
        }

        private void btn_saveNew_Click(object sender, RoutedEventArgs e)
        {
            nvm.AddAndSaveChanges(dg, sifra_normativa, naziv_artikla, naziv_normativa, kolicina, btn_save, btn_cancel, btn_remove);
        }

        private void dg_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            nvm.ItemSelected(dg, sifra_normativa, naziv_artikla, naziv_normativa, kolicina, btn_save, btn_cancel, btn_remove);
        }

        private void kolicina_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
                nvm.AddAndSaveChanges(dg, sifra_normativa, naziv_artikla, naziv_normativa, kolicina, btn_save, btn_cancel, btn_remove);
        }
    }
}
