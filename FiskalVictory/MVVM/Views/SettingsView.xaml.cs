﻿using FiskalVictory.MVVM.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FiskalVictory.MVVM.Views
{
    /// <summary>
    /// Interaction logic for SettingsView.xaml
    /// </summary>
    public partial class SettingsView : UserControl
    {
        public SettingsView()
        {
            InitializeComponent();
        }

        SettingsViewModel svm = new SettingsViewModel();

        private void btnArhiviraj_Click(object sender, RoutedEventArgs e)
        {
            svm.archiveLocally();
        }

        private void btnArhivirajEmail_Click(object sender, RoutedEventArgs e)
        {
            
        }
    }
}
