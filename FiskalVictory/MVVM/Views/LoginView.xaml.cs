﻿using FiskalVictory.Core;
using FiskalVictory.MVVM.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FiskalVictory.MVVM.Views
{
    /// <summary>
    /// Interaction logic for LoginView.xaml
    /// </summary>
    public partial class LoginView : UserControl
    {
        LoginViewModel loginvm = new LoginViewModel();

        public LoginView()
        {
            InitializeComponent();
            Loaded += focusTBpassword;
        }
        private void focusTBpassword(object sender, RoutedEventArgs e)
        {
            tb_lozinka.Focus();
        }

        private void btn_login_Click(object sender, RoutedEventArgs e)
        {
            checkForPasswordAndLicence();
        }

        private void tb_lozinka_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                checkForPasswordAndLicence();
            }
        }

        public void checkForPasswordAndLicence()
        {
            loginvm.CheckForLogin(tb_lozinka);
        }
    }
}
