﻿using FiskalVictory.MVVM.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FiskalVictory.MVVM.Views
{
    /// <summary>
    /// Interaction logic for SpeedDialView.xaml
    /// </summary>
    public partial class SpeedDialView : UserControl
    {
        
        public SpeedDialView()
        {
            InitializeComponent();
        }

        private void speedDialProducts_MouseUp(object sender, MouseButtonEventArgs e)
        {
            HomeView homeView = HomeView.HomeV;
            homeView.ProductsPressed();
        }
    }
}
