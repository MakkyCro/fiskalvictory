﻿using FiskalVictory.DataAccess;
using FiskalVictory.MVVM.Models;
using FiskalVictory.MVVM.ViewModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace FiskalVictory.MVVM.Views
{
    /// <summary>
    /// Interaction logic for IzdavanjeRacunaView.xaml
    /// </summary>
    public partial class IzdavanjeRacunaView : UserControl
    {
        public IzdavanjeRacunaView()
        {
            InitializeComponent();
        }
        IzdavanjeRacunaViewModel i = new IzdavanjeRacunaViewModel();

        private void izdavanjeRacuna_Loaded(object sender, RoutedEventArgs e)
        {
            i.loadNacinPlacanja(nacin_placanja);
            i.LoadedIzdavanje(dg_stavke, wp_Grupe, wp_Artikli, sp_Racun, nacin_placanja, btnBrisanje, btnIzdavanje);
        }

        private void btnIzdavanje_Click(object sender, RoutedEventArgs e)
        {
            i.izdajRacun(dg_stavke, btnIzdavanje,btnBrisanje,nacin_placanja, sp_Racun);
        }

        private void dg_stavke_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            i.odaberiStavku(dg_stavke, btnIzdavanje, btnBrisanje, nacin_placanja);
        }

      

        private void btnBrisanje_Click(object sender, RoutedEventArgs e)
        {
            i.obrisiStavku(dg_stavke);
        }

        //private void btnKolicina_Click(object sender, RoutedEventArgs e)
        //{
        //    i.smanjiKolicinu(dg_stavke);
        //}
    }
}
