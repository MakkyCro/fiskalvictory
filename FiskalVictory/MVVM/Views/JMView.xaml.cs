﻿using FiskalVictory.MVVM.ViewModels;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace FiskalVictory.MVVM.Views
{
    /// <summary>
    /// Interaction logic for JMView.xaml
    /// </summary>
    public partial class JMView : UserControl
    {
        JMViewModel jmvm = new JMViewModel();
        public JMView()
        {
            InitializeComponent();
        }

        private void dg_workers_Loaded(object sender, RoutedEventArgs e)
        {
            jmvm.LoadJM(dg);
        }

        private void btn_AddWorker_Click(object sender, RoutedEventArgs e)
        {
            jmvm.New(dg, sifra_JM, naziv_JM, btn_save, btn_cancel);
        }

        private void btn_removeWorker_Click(object sender, RoutedEventArgs e)
        {
            jmvm.Remove(dg, sifra_JM, naziv_JM, btn_save, btn_cancel, btn_remove);
        }

        private void btn_CancelChangesWorker_Click(object sender, RoutedEventArgs e)
        {
            jmvm.CancelNewEntry(dg, sifra_JM, naziv_JM, btn_save, btn_cancel, btn_remove);
        }

        private void btn_saveNewWorker_Click(object sender, RoutedEventArgs e)
        {
            jmvm.AddAndSaveChanges(dg, sifra_JM, naziv_JM, btn_save, btn_cancel, btn_remove);
        }

        private void dg_workers_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            jmvm.ItemSelected(dg, sifra_JM, naziv_JM, btn_save, btn_cancel, btn_remove);
        }

        private void naziv_JM_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if(e.Key == Key.Enter)
                jmvm.AddAndSaveChanges(dg, sifra_JM, naziv_JM, btn_save, btn_cancel, btn_remove);
        }
    }
}
