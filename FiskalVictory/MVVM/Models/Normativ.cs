﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace FiskalVictory.MVVM.Models
{
    public class Normativ
    {
        [Key]
        [Column(Order = 1)]
        public int sifra_normativa { get; set; }
        [Key]
        [Column(Order = 2)]
        public int sifra_artikla { get; set; }
        public virtual Artikl Artikl { get; set; }

        [Required]
        [MaxLength(60)]
        public string naziv_normativa { get; set; }

        [Required]
        public double kolicina { get; set; }
    }
}
