﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace FiskalVictory.MVVM.Models
{
    public class JedinicaMjere
    {
        [Key]
        public int sifra_JM { get; set; }

        [Required]
        [MaxLength(50)]
        public string naziv_JM { get; set; }
        public virtual ICollection<Artikl> Artikl { get; private set; } = new ObservableCollection<Artikl>();
    }
}
