﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace FiskalVictory.MVVM.Models
{
    public class Kategorija
    {
        [Key]
        public int sifra_kategorije { get; set; }

        [Required]
        [MaxLength(50)]
        public string naziv_kategorije { get; set; }

        [Required]
        [MaxLength(3)]
        public int PDV { get; set; }

        [Required]
        [MaxLength(3)]
        public int PNP { get; set; }

        [MaxLength(100)]
        public string opis { get; set; }
        public virtual ICollection<Artikl> Artikl { get; private set; } = new ObservableCollection<Artikl>();
    }
}
