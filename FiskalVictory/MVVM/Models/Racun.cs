﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace FiskalVictory.MVVM.Models
{
    public class Racun
    {
        [Key]
        public string sifra_racuna { get; set; }


        public string datum_izdavanja { get; set; }

        public string vrijeme_izdavanja { get; set; }

        public double ukupna_cijena { get; set; }

        public string JIR { get; set; }

        public string ZK { get; set; }

        public bool fiskaliziran { get; set; }

        public string oibRadnika { get; set; }
        public virtual Radnik Radnik { get; set; }

        public int nacinPlacanjaID { get; set; }
        
        public virtual NacinPlacanja nacinPlacanja { get; set; }

    }
}
