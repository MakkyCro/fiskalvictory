﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace FiskalVictory.MVVM.Models
{
    public class Stavka
    {
        [Key]
        [Column(Order = 1)]
        public int rb_stavke { get; set; }

        [Key]
        [Column(Order = 2)]
        public string sifra_racuna { get; set; }
        public virtual Racun Racun { get; set; }

        [Required]
        public int kolicina { get; set; }
        [Required]
        public double cijena_stavke { get; set; }

        [Required]
        public int artiklID { get; set; }
        public virtual Artikl Artikl { get; set; }


    }
}
