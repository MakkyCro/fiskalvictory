﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;

namespace FiskalVictory.MVVM.Models
{
    public class Artikl
    {
        [Key]
        public int sifra_artikla { get; set; }
        
        [Required]
        [MaxLength(60)]
        public string ime_artikla { get; set; }
        
        [Required]
        public double osnovica { get; set; }

        public string putanja_do_slike { get; set; }

        [Required]
        public bool vidljivost { get; set; }


        public int JedinicaMjereID { get; set; }
        public virtual JedinicaMjere JedinicaMjere { get; set; }

        public int grupaID { get; set; }
        public virtual Grupa Grupa { get; set; }

        public int kategorijaID { get; set; }
        public virtual Kategorija Kategorija { get; set; }

        public virtual ICollection<Stavka> stavka { get; private set; } = new ObservableCollection<Stavka>();
    }
}
