﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace FiskalVictory.MVVM.Models
{
    public class NacinPlacanja
    {
        [Key]
        public int sifra_placanja { get; set; }
        
        [Required]
        [MaxLength(100)]
        public string naziv_placanja { get; set; }

        public virtual ICollection<Racun> Racun { get; private set; } = new ObservableCollection<Racun>();
    }
}
