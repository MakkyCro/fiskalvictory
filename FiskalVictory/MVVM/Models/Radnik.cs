﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace FiskalVictory.MVVM.Models
{
    public class Radnik
    {
        [Key]
        [MaxLength(11)]
        public string oib_radnika { get; set; }
        [Required]
        [MaxLength(60)]
        public string ime_radnika { get; set; }
        [Required]
        [MaxLength(60)]
        public string prezime_radnika { get; set; }
        [Required]
        [MaxLength(60)]
        public string lozinka_radnika { get; set; }
        [MaxLength(60)]
        public string smjena { get; set; }
        [MaxLength(60)]
        public string broj_telefona { get; set; }

        public virtual ICollection<Racun> Racun { get; private set; } = new ObservableCollection<Racun>();
    }
}
