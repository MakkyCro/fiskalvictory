﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace FiskalVictory.MVVM.Models
{
    public class Grupa
    {
        [Key]
        public int sifra_grupe { get; set; }

        [Required]
        [MaxLength(50)]
        public string naziv_grupe { get; set; }

        public virtual ICollection<Artikl> Artikl { get; private set; } = new ObservableCollection<Artikl>();
    }
}
