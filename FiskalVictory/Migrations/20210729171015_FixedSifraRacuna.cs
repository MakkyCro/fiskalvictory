﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace FiskalVictory.Migrations
{
    public partial class FixedSifraRacuna : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Stavke_Racuni_Racunsifra_racuna",
                table: "Stavke");

            migrationBuilder.DropIndex(
                name: "IX_Stavke_Racunsifra_racuna",
                table: "Stavke");

            migrationBuilder.DropColumn(
                name: "Racunsifra_racuna",
                table: "Stavke");

            migrationBuilder.AlterColumn<string>(
                name: "sifra_racuna",
                table: "Stavke",
                type: "TEXT",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "INTEGER");

            migrationBuilder.CreateIndex(
                name: "IX_Stavke_sifra_racuna",
                table: "Stavke",
                column: "sifra_racuna");

            migrationBuilder.AddForeignKey(
                name: "FK_Stavke_Racuni_sifra_racuna",
                table: "Stavke",
                column: "sifra_racuna",
                principalTable: "Racuni",
                principalColumn: "sifra_racuna",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Stavke_Racuni_sifra_racuna",
                table: "Stavke");

            migrationBuilder.DropIndex(
                name: "IX_Stavke_sifra_racuna",
                table: "Stavke");

            migrationBuilder.AlterColumn<int>(
                name: "sifra_racuna",
                table: "Stavke",
                type: "INTEGER",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "TEXT");

            migrationBuilder.AddColumn<string>(
                name: "Racunsifra_racuna",
                table: "Stavke",
                type: "TEXT",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Stavke_Racunsifra_racuna",
                table: "Stavke",
                column: "Racunsifra_racuna");

            migrationBuilder.AddForeignKey(
                name: "FK_Stavke_Racuni_Racunsifra_racuna",
                table: "Stavke",
                column: "Racunsifra_racuna",
                principalTable: "Racuni",
                principalColumn: "sifra_racuna",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
