﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace FiskalVictory.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Grupa",
                columns: table => new
                {
                    sifra_grupe = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    naziv_grupe = table.Column<string>(type: "TEXT", maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Grupa", x => x.sifra_grupe);
                });

            migrationBuilder.CreateTable(
                name: "Jed_mj",
                columns: table => new
                {
                    sifra_JM = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    naziv_JM = table.Column<string>(type: "TEXT", maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Jed_mj", x => x.sifra_JM);
                });

            migrationBuilder.CreateTable(
                name: "Kategorija",
                columns: table => new
                {
                    sifra_kategorije = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    naziv_kategorije = table.Column<string>(type: "TEXT", maxLength: 50, nullable: false),
                    PDV = table.Column<int>(type: "INTEGER", maxLength: 3, nullable: false),
                    PNP = table.Column<int>(type: "INTEGER", maxLength: 3, nullable: false),
                    opis = table.Column<string>(type: "TEXT", maxLength: 100, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Kategorija", x => x.sifra_kategorije);
                });

            migrationBuilder.CreateTable(
                name: "NacinPlacanja",
                columns: table => new
                {
                    sifra_placanja = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    naziv_placanja = table.Column<string>(type: "TEXT", maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NacinPlacanja", x => x.sifra_placanja);
                });

            migrationBuilder.CreateTable(
                name: "Radnici",
                columns: table => new
                {
                    oib_radnika = table.Column<string>(type: "TEXT", maxLength: 11, nullable: false),
                    ime_radnika = table.Column<string>(type: "TEXT", maxLength: 60, nullable: false),
                    prezime_radnika = table.Column<string>(type: "TEXT", maxLength: 60, nullable: false),
                    lozinka_radnika = table.Column<string>(type: "TEXT", maxLength: 60, nullable: false),
                    smjena = table.Column<string>(type: "TEXT", maxLength: 60, nullable: true),
                    broj_telefona = table.Column<string>(type: "TEXT", maxLength: 60, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Radnici", x => x.oib_radnika);
                });

            migrationBuilder.CreateTable(
                name: "Artikli",
                columns: table => new
                {
                    sifra_artikla = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    ime_artikla = table.Column<string>(type: "TEXT", maxLength: 60, nullable: false),
                    osnovica = table.Column<double>(type: "REAL", nullable: false),
                    putanja_do_slike = table.Column<string>(type: "TEXT", nullable: true),
                    vidljivost = table.Column<bool>(type: "INTEGER", nullable: false),
                    JedinicaMjereID = table.Column<int>(type: "INTEGER", nullable: false),
                    grupaID = table.Column<int>(type: "INTEGER", nullable: false),
                    kategorijaID = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Artikli", x => x.sifra_artikla);
                    table.ForeignKey(
                        name: "FK_Artikli_Grupa_grupaID",
                        column: x => x.grupaID,
                        principalTable: "Grupa",
                        principalColumn: "sifra_grupe",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Artikli_Jed_mj_JedinicaMjereID",
                        column: x => x.JedinicaMjereID,
                        principalTable: "Jed_mj",
                        principalColumn: "sifra_JM",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Artikli_Kategorija_kategorijaID",
                        column: x => x.kategorijaID,
                        principalTable: "Kategorija",
                        principalColumn: "sifra_kategorije",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Racuni",
                columns: table => new
                {
                    sifra_racuna = table.Column<string>(type: "TEXT", nullable: false),
                    datum_izdavanja = table.Column<string>(type: "TEXT", nullable: false),
                    vrijeme_izdavanja = table.Column<string>(type: "TEXT", nullable: false),
                    ukupna_cijena = table.Column<double>(type: "REAL", nullable: false),
                    JIR = table.Column<string>(type: "TEXT", nullable: false),
                    ZK = table.Column<string>(type: "TEXT", nullable: false),
                    fiskaliziran = table.Column<bool>(type: "INTEGER", nullable: false),
                    oibRadnika = table.Column<string>(type: "TEXT", nullable: true),
                    Radnikoib_radnika = table.Column<string>(type: "TEXT", nullable: true),
                    nacinPlacanjaID = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Racuni", x => x.sifra_racuna);
                    table.ForeignKey(
                        name: "FK_Racuni_NacinPlacanja_nacinPlacanjaID",
                        column: x => x.nacinPlacanjaID,
                        principalTable: "NacinPlacanja",
                        principalColumn: "sifra_placanja",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Racuni_Radnici_Radnikoib_radnika",
                        column: x => x.Radnikoib_radnika,
                        principalTable: "Radnici",
                        principalColumn: "oib_radnika",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Normativi",
                columns: table => new
                {
                    sifra_normativa = table.Column<int>(type: "INTEGER", nullable: false),
                    sifra_artikla = table.Column<int>(type: "INTEGER", nullable: false),
                    naziv_normativa = table.Column<string>(type: "TEXT", maxLength: 60, nullable: false),
                    kolicina = table.Column<double>(type: "REAL", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Normativi", x => new { x.sifra_normativa, x.sifra_artikla });
                    table.ForeignKey(
                        name: "FK_Normativi_Artikli_sifra_artikla",
                        column: x => x.sifra_artikla,
                        principalTable: "Artikli",
                        principalColumn: "sifra_artikla",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Stavke",
                columns: table => new
                {
                    rb_stavke = table.Column<int>(type: "INTEGER", nullable: false),
                    sifra_racuna = table.Column<int>(type: "INTEGER", nullable: false),
                    Racunsifra_racuna = table.Column<string>(type: "TEXT", nullable: true),
                    kolicina = table.Column<int>(type: "INTEGER", nullable: false),
                    cijena_stavke = table.Column<double>(type: "REAL", nullable: false),
                    cijena_bez_PDV = table.Column<double>(type: "REAL", nullable: false),
                    artiklID = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Stavke", x => new { x.rb_stavke, x.sifra_racuna });
                    table.ForeignKey(
                        name: "FK_Stavke_Artikli_artiklID",
                        column: x => x.artiklID,
                        principalTable: "Artikli",
                        principalColumn: "sifra_artikla",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Stavke_Racuni_Racunsifra_racuna",
                        column: x => x.Racunsifra_racuna,
                        principalTable: "Racuni",
                        principalColumn: "sifra_racuna",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Artikli_grupaID",
                table: "Artikli",
                column: "grupaID");

            migrationBuilder.CreateIndex(
                name: "IX_Artikli_JedinicaMjereID",
                table: "Artikli",
                column: "JedinicaMjereID");

            migrationBuilder.CreateIndex(
                name: "IX_Artikli_kategorijaID",
                table: "Artikli",
                column: "kategorijaID");

            migrationBuilder.CreateIndex(
                name: "IX_Normativi_sifra_artikla",
                table: "Normativi",
                column: "sifra_artikla");

            migrationBuilder.CreateIndex(
                name: "IX_Racuni_nacinPlacanjaID",
                table: "Racuni",
                column: "nacinPlacanjaID");

            migrationBuilder.CreateIndex(
                name: "IX_Racuni_Radnikoib_radnika",
                table: "Racuni",
                column: "Radnikoib_radnika");

            migrationBuilder.CreateIndex(
                name: "IX_Stavke_artiklID",
                table: "Stavke",
                column: "artiklID");

            migrationBuilder.CreateIndex(
                name: "IX_Stavke_Racunsifra_racuna",
                table: "Stavke",
                column: "Racunsifra_racuna");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Normativi");

            migrationBuilder.DropTable(
                name: "Stavke");

            migrationBuilder.DropTable(
                name: "Artikli");

            migrationBuilder.DropTable(
                name: "Racuni");

            migrationBuilder.DropTable(
                name: "Grupa");

            migrationBuilder.DropTable(
                name: "Jed_mj");

            migrationBuilder.DropTable(
                name: "Kategorija");

            migrationBuilder.DropTable(
                name: "NacinPlacanja");

            migrationBuilder.DropTable(
                name: "Radnici");
        }
    }
}
