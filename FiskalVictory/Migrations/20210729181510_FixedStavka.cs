﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace FiskalVictory.Migrations
{
    public partial class FixedStavka : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "cijena_bez_PDV",
                table: "Stavke");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<double>(
                name: "cijena_bez_PDV",
                table: "Stavke",
                type: "REAL",
                nullable: false,
                defaultValue: 0.0);
        }
    }
}
