﻿using System.Windows;
using FiskalVictory.Core;
using FiskalVictory.MVVM.ViewModels;
using System.Diagnostics;

namespace FiskalVictory
{
    /// <summary>
    /// Interaction logic for FiskalVictoryWindow.xaml
    /// </summary>
    public partial class FiskalVictoryWindow : Window
    {

        public string user = "";
        public static readonly FiskalVictoryWindow mainWindowFV = new FiskalVictoryWindow();

        //private FiskalVictoryWindow() { }

        public static FiskalVictoryWindow MainWindowFV
        {
            get
            {
                return mainWindowFV;
            }
        }

        Licence licenca = Licence.Lic;
        ClockManager clock = ClockManager.Clock;
        IsConnected checkInternet = new IsConnected();
        private FiskalVictoryWindow()
        {
            InitializeComponent();

            // Postavljanje sifre kod debuggiranja
            if (Debugger.IsAttached)
            {
                userdata.Default.sifra_admina = "admin";
            }

            var viewModel = (MainViewModel)DataContext;

            // Prikaži verziju programa
            tb_version1.Text = userdata.Default.verzija;
            // Pokreni timer za osvježavanje vremena i datuma
            clock.startTimer(tb_time,tb_date);

            if (checkInternet.IsConnectedToInternet())
            {
                // Provjera postoji li licenca u bazi podataka
                if (licenca.checkLicence())
                {
                    // Učitavanje logina ako postoji licenca
                    viewModel.LoginVMCommand.Execute(null);

                    userdata.Default.licenca = licenca.getLicenca();
                    userdata.Default.licenca_datum_od = licenca.getDatumOd();
                    userdata.Default.licenca_datum_do = licenca.getDatumDo();
                    userdata.Default.Save();
                    userdata.Default.Reload();
                }
                else
                {
                    if (licenca.checkIfDeviceInDB())
                    {
                        MessageBox.Show("Licenca za Vaš uređaj još nije aktivna. ID uređaja: " + userdata.Default.id_uredaja);
                    }
                    else
                    {
                        // Učitavanje registracije ako ne postoji licenca
                        viewModel.RegisterUserVMCommand.Execute(null);
                    }
                }
            }
            else
            {
                if (!licenca.checkLicenceOffline())
                {
                    if (licenca.checkDatumOffline())
                        MessageBox.Show("Za prvo paljenje programa potrebno je imati internetsku vezu kako biste preuzeli licencu.");
                    else
                        MessageBox.Show("Ukoliko ste novi korisnik trebate napraviti registraciju uređaja.");
                }
                else
                {

                    // Učitavanje logina ako postoji licenca
                    viewModel.LoginVMCommand.Execute(null);
                }
            }
        }
        public void RegisterPressed()
        {
            var viewModel = (MainViewModel)DataContext;
            viewModel.LoginVMCommand.Execute(null);
        }
        public void LoginPressed()
        {
            var viewModel = (MainViewModel)DataContext;
            viewModel.HomeVMCommand.Execute(null);
        }

        public void LogoutPressed()
        {
            var viewModel = (MainViewModel)DataContext;
            viewModel.LoginVMCommand.Execute(null);
        }
    }
}
