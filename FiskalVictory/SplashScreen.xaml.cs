﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;
using FiskalVictory.Core;

namespace FiskalVictory
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        // Učitavanje klasa za pristupanje metodama
        IsConnected network = new IsConnected();
        SelectFromDB getFromDB = SelectFromDB.GetFromDataBase;
        Licence lic = Licence.Lic;

        // Varijable za izvršavanje querya
        string q_getVersion = "select verzija from verzija order by sifra_verzije desc limit 1;";
        public MainWindow()
        {
            InitializeComponent();
            if (userdata.Default.verzija == "")
            {
                userdata.Default.verzija = "v0.0.1";
                userdata.Default.Save();
                userdata.Default.Reload();
            }

            // Generiraj ID računala za provjeru licence
            lic.generateDeviceID();

            // Osvježavanje statusa
            tb_status.Text = "Provjera internetske veze";

            // Ukoliko ima internetske veze
            if (network.IsConnectedToInternet())
            {
                // Pomakni loadbar
                loadbar.Value += 50;
                // Spremi verziju u settings
                string tmpVerzija = getFromDB.GetValueFromDB(q_getVersion);

                // Provjera ima li ažuriranja na temelju verzije programa
                tb_status.Text = "Provjera ima li novijih verzija programa";
                if (tmpVerzija != userdata.Default.verzija)
                {
                    // Ažuriranje verzije u userdata
                    userdata.Default.verzija = tmpVerzija;
                    
                    userdata.Default.Save();
                    userdata.Default.Reload();

                    // Preuzimanje novije verzije programa
                    ProcessStartInfo info = new ProcessStartInfo(System.Reflection.Assembly.GetExecutingAssembly().Location.Replace("FiskalVictory.dll", "")+"UpdateManager\\UpdateManager.exe");
                    info.UseShellExecute = true;
                    info.Verb = "runas";
                    // Restart aplikacije
                    Process.Start(info);
                }
                // Osvježi verziju na splash screenu
                tb_version.Text = userdata.Default.verzija;

                // Pomakni loadbar
                loadbar.Value += 50;

                // Osvježavanje statusa
                tb_status.Text = "Učitavanje programa";

            }
            // Ukoliko nema internetske veze
            else
            {
                MessageBox.Show("Nema internetske veze. Nemogućnost provjere potencijalnih ažuriranja.");
            }

            startTimer();
        }

        DispatcherTimer timer = new DispatcherTimer();
        int counter = 0;
        public void startTimer()
        {
            //Izrada novog timera
            //Interval osvježavanja je sekunda
            //Dodaj metodu za tick
            //Pokreni timer
            timer.Interval = TimeSpan.FromSeconds(1);
            timer.Tick += timer_Tick;
            timer.Start();
        }
        void timer_Tick(object sender, EventArgs e)
        {
            counter++;
            if (counter == 2)
            {
                timer.Stop();
                try
                {
                    this.Hide();
                    FiskalVictoryWindow fv = FiskalVictoryWindow.MainWindowFV;
                    fv.Show();
                    this.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Problem kod pokretanja aplikacije!", ex.ToString());
                }
            }
           
        }

    }

}
