﻿using System;
using System.Diagnostics;
using System.IO;
using System.Net;

namespace UpdateManager
{
    class Program
    {
        static void Main(string[] args)
        {
            string path = System.Reflection.Assembly.GetExecutingAssembly().Location.Replace("UpdateManager.dll", "") + "tmp";

            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);

            foreach (var process in Process.GetProcessesByName("FiskalVictory"))
            {
                process.Kill();
            }

            Console.WriteLine("Početak preuzimanja...");
            using (var client = new WebClient())
            {
                client.DownloadFile("http://165.227.146.167/Release/FiskalVictory.zip", path + "\\FiskalVictory.zip");

            }
            Console.WriteLine("Preuzimanje - završeno!");

            Console.WriteLine("Raspakiravanje datoteka...");
            System.IO.Compression.ZipFile.ExtractToDirectory(path + "\\FiskalVictory.zip", path.Replace("UpdateManager\\tmp", ""), true);
            Console.WriteLine("Raspakiravanje - završeno!");
            Console.WriteLine("Ažuriranje aplikacije - završeno!");
            Console.WriteLine("Brisanje preuzetih datoteka...");
            Directory.Delete(path, true);
            Console.WriteLine("Brisanje - Završeno!");
            Console.WriteLine("Ponovno pokretanje programa...");
            Console.ReadLine();

            ProcessStartInfo info = new ProcessStartInfo(path.Replace("UpdateManager\\tmp", "") + "FiskalVictory.exe");
            info.UseShellExecute = true;

            // Restart aplikacije
            Process.Start(info);
        }
    }
}
